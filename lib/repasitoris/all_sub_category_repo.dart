
import 'package:vanfood/api_config/api.dart';
import '../api_config/consturl.dart';
import '../api_config/consturl_keys.dart';
import '../models/all_sub_category_model.dart';

class AllSubCategorys {

  getAllSubCategory() async {
    return await RequestGet.getData(
      AppUrl.app_all_subcategories +
          AppUrl.limit +
          AppUrlKeys.limit.toString() +
          AppUrl.offset +
          AppUrlKeys.offset.toString() +
          AppUrl.cat_id +
          AppUrlKeys.cat_id.toString(),
      {},
    );
  }
}
