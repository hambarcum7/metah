import 'package:firebase_auth/firebase_auth.dart';

class LoginService {
  bool hasError = false;
  String errorText = '';
  String userVerificationId = '';

  Future<void> signInWithEmail(String email, String password) async {
    try {
      await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      hasError = false;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        hasError = true;
        errorText = 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        hasError = true;
        errorText = 'Wrong password provided for that user.';
      }
    }
  }

  Future<void> signUpWithEmail(String email, String password) async {
    try {
      await FirebaseAuth.instance
          .createUserWithEmailAndPassword(email: email, password: password);
      hasError = false;
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        errorText = 'The password provided is too weak.';
        hasError = true;
      } else if (e.code == 'email-already-in-use') {
        hasError = true;
        errorText = 'The account already exists for that email.';
      }
    }
  }

  Future<void> verifyNumber(String phoneNumber) async {
    try {
      await FirebaseAuth.instance.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: const Duration(seconds: 60),
        verificationCompleted: (PhoneAuthCredential credential) async {
          await FirebaseAuth.instance.signInWithCredential(credential);
        },
        verificationFailed: (FirebaseAuthException e) {},
        codeSent: (String verificationID, int? resendToken) {
          UserVerificationId.verificationId = verificationID;
        },
        codeAutoRetrievalTimeout: (String verificationID) {
          UserVerificationId.verificationId = verificationID;
        },
      );
    } on FirebaseAuthException catch (e) {
      hasError = true;
      errorText = e.code;
    }
  }

  Future<void> signinWithTelephone(String smsCode) async {
    try {
      await FirebaseAuth.instance.signInWithCredential(
        PhoneAuthProvider.credential(
          verificationId: UserVerificationId.verificationId,
          smsCode: smsCode,
        ),
      );
      hasError = false;
      errorText = '';
    } on FirebaseAuthException catch (e) {
      hasError = true;
      errorText = e.code;
    }
  }

  Future<void> logOut() async {
    await FirebaseAuth.instance.signOut();
  }
}

class UserVerificationId {
  static String verificationId = '';
}