

import '../api_config/api.dart';
import '../api_config/consturl.dart';
import '../api_config/consturl_keys.dart';

class ProductColections{
  getProductColections() async {
    return await RequestGet.getData(AppUrl.app_product_colection+AppUrl.limit+AppUrlKeys.limit.toString()+AppUrl.offset+AppUrlKeys.offset.toString(), {});
  }
}