

import '../api_config/api.dart';
import '../api_config/consturl.dart';
import '../api_config/consturl_keys.dart';

class AllProducts{
  getAllProducts() async {
    return await RequestGet.postData(AppUrl.app_all_products+AppUrl.limit+AppUrlKeys.limit.toString()+AppUrl.offset+AppUrlKeys.offset.toString(),{},
    );
  }
}