import 'package:firebase_auth/firebase_auth.dart';

class CurrentUserService {
  bool hasCurrentUser = false;
  User? currentUser;

  void getCurrentUserState() {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user == null) {
        hasCurrentUser = false;
      } else {
        hasCurrentUser = true;
        currentUser = user;
      }
    });
  }
}
