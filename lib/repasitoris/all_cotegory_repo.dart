import 'package:vanfood/api_config/api.dart';
import '../api_config/consturl.dart';
import '../api_config/consturl_keys.dart';

class AllCategoris {
  getAllCategoris() async {
    return await RequestGet.postData(
        AppUrl.app_all_categories +
            AppUrl.limit +
            AppUrlKeys.limit.toString() +
            AppUrl.offset +
            AppUrlKeys.offset.toString(),
        {});
  }
}
