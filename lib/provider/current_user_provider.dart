import 'package:flutter/material.dart';
import 'package:vanfood/repasitoris/current_user_service.dart';

class CurrentUserProvider extends ChangeNotifier{
  CurrentUserService service = CurrentUserService();

  getCurrentUserState(){
    service.getCurrentUserState();
    notifyListeners();
  }
}