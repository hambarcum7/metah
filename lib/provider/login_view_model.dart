import 'package:flutter/material.dart';
import '../repasitoris/login_service.dart';

class LoginViewModel extends ChangeNotifier {
  LoginService service = LoginService();

  Future<void> signInWithEmail(email, password) async {
    await service.signInWithEmail(email, password);
    service = LoginService();
    notifyListeners();
  }

  Future<void> signUpWithEmail(email, password) async{
    await service.signUpWithEmail(email, password);
    service = LoginService();
    notifyListeners();
  }

  Future<void> verifyNumber(String phoneNumber) async {
    service = LoginService();
    await service.verifyNumber(phoneNumber);
    notifyListeners();
  }

  Future<void> signinWithTelephone(String smsCode) async {
    service = LoginService();
    await service.signinWithTelephone(smsCode);
    notifyListeners();
  }

  Future<void> logOut() async {
    service = LoginService();
    await service.logOut();
    notifyListeners();
  }
}