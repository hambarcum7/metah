import 'dart:convert';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/main.dart';
import 'package:vanfood/provider/provider_all_products.dart';
import 'package:vanfood/models/all_sub_category_model.dart';
import 'package:vanfood/repasitoris/all_cotegory_repo.dart';
import 'package:vanfood/ui/screen/card/foryou_card.dart';
import '../../../models/all_cotegory_model.dart';
import 'package:vanfood/repasitoris/all_products_repo.dart';
import 'package:vanfood/ui/screen/detail/detail.dart';
import 'package:vanfood/ui/screen/login_page/login_page.dart';
import 'package:vanfood/ui/screen/product_detail/product_detail.dart';
import '../../../models/all_products/all_products.dart';
import '../../../models/product_colections_model.dart';
import '../../../provider/current_user_provider.dart';
import '../../../provider/provider_all_categorys.dart';
import '../../../provider/provider_all_sub_categorys.dart';
import '../../../repasitoris/current_user_service.dart';
import '../../components/colors.dart';
import '../../components/popup_menu.dart';
import '../../components/space.dart';
import '../../components/widgets.dart';
import '../card/card.dart';
import '../salades_page/salades_page.dart';

// ignore: must_be_immutable
class Special extends StatefulWidget {
  Special({Key? key}) : super(key: key);

  @override
  State<Special> createState() => SpecialProvider();
}

class SpecialProvider extends State<Special> {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => ViewModelAllProducts(),
        ),
        ChangeNotifierProvider(
          create: (context) => CurrentUserProvider(),
        )
      ],
      child: _SpecialState(),
    );
  }
}

class _SpecialState extends StatefulWidget {
  @override
  State<_SpecialState> createState() => _SpecialStateState();
}

late Future<List<Allproducts>> data;

class _SpecialStateState extends State<_SpecialState> {
  Future<List<Allproducts>> init() async {
    if (connectionStatus == ConnectivityResult.wifi ||
        connectionStatus == ConnectivityResult.wifi) {
      final List<dynamic> listSpecial =
          await VeiwModelAllCategory().getSpecialInfo() as List;
      final allProductsSpecial = listSpecial
          .map((e) => Allproducts.fromJson(e as Map<String, dynamic>))
          .toList();
      print(allProductsSpecial[0].name);

      // final List<AllCategory> allCategorySpecial =  listSpecial.map((e) => AllCategory.fromJson(e)).toList();
      //     final AllCategory allCategorySpecial = VeiwModelAllCategory().getSpecialInfo();
      //    print(allCategorySpecial[0].addedDate);
      return allProductsSpecial;
    } else {
      throw ('Error');
    }
  }

  @override
  void initState() {
    data = init();
    data.then((value) => print('gjjh' + value[0].toString()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('build speishal');
    List widget = <Widget>[
      widgetrest(context, 'home_restaurants'.tr(), 'assets/img/restorant.png'),
      widgetrest(context, 'home_cafe'.tr(), 'assets/img/restorant.png'),
      widgetrest(context, 'home_shaurma'.tr(), 'assets/img/restorant.png'),
    ];
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      locale: context.locale,
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          iconTheme: IconThemeData(color: AppColors.mainColorDark),
          backgroundColor: Colors.white,
          actions: [
            Center(
              child: Container(
                margin: const EdgeInsets.only(
                  right: 13,
                ),
                child: Stack(
                  children: [
                    Image.asset(
                      'assets/Vector.png',
                    ),
                    Align(
                      heightFactor: 1,
                      widthFactor: 0.9,
                      child: Container(
                        width: 7,
                        height: 7,
                        decoration: BoxDecoration(
                          color: AppColors.activeRed,
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        body: Container(
          padding: const EdgeInsets.only(top: 10),
          width: double.maxFinite,
          height: double.maxFinite,
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(
                    left: 13,
                    right: 13,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // top text
                      containerText('home_helloUser'.tr(), 12,
                          AppColors.mainColorDark, 8, FontWeight.w700),
                      containerText('home_areYouHungry'.tr(), 15,
                          AppColors.mainColorDark, 8, FontWeight.w900),

                      containerText('home_findYourFood'.tr(), 15,
                          AppColors.mainColorDark, 0, FontWeight.w900),
                      //search
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(
                              top: Space.space15,
                            ),
                            width: MediaQuery.of(context).size.width - 40,
                            height: 40,
                            child: Form(
                              // key: _formKey,
                              child: Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: TextField(
                                      textAlignVertical:
                                          TextAlignVertical.center,
                                      decoration: InputDecoration(
                                        hintText: 'home_foodSearch'.tr(),
                                        hintStyle: TextStyle(
                                          fontSize: 13,
                                          color: AppColors.searchColor,
                                        ),
                                        contentPadding: const EdgeInsets.only(
                                            left: 0,
                                            bottom: 15,
                                            top: 0,
                                            right: 0),
                                        border: InputBorder.none,
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 2,
                                    child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          IconButton(
                                              onPressed: () => setState(() {
                                                    context.setLocale(
                                                        const Locale('hy'));
                                                  }),
                                              icon: const Icon(Icons.search)),
                                          Container(
                                            margin: const EdgeInsets.only(
                                              left: 6,
                                            ),
                                            alignment: Alignment.center,
                                            width: 56,
                                            height: 40,
                                            decoration: BoxDecoration(
                                              color: AppColors.mainColorLight,
                                              borderRadius:
                                                  const BorderRadius.only(
                                                topRight: Radius.circular(8),
                                                bottomRight: Radius.circular(8),
                                              ),
                                            ),
                                            child: Image.asset(
                                              'assets/input_icon.png',
                                            ),
                                          ),
                                        ]),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: List.generate(
                            4,
                            (index) => widgetProduct(
                              context,
                              () {
                                CurrentUserProvider model =
                                    Provider.of<CurrentUserProvider>(
                                  context,
                                  listen: false,
                                );
                                model.getCurrentUserState();
                                CurrentUserService service = model.service;
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => service.hasCurrentUser
                                        ? const ProductDetail()
                                        : LoginPage(),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                          height: 60,
                          child: ListView.builder(
                              itemCount: widget.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, index) => widget[index])),
                    ],
                  ),
                ),
                Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      homeBoldText('category_salades'.tr()),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const SaladePage(),
                            ),
                          );
                        },
                        child: Text(
                          'home_seeAll'.tr(),
                          style: TextStyle(
                              color: AppColors.unActive,
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 260,
                  child: FutureBuilder<List<Allproducts>>(
                      future: data,
                      builder:
                          (context, AsyncSnapshot<List<Allproducts>> snapshot) {
                        if (!snapshot.hasData) {
                          return const CircularProgressIndicator();
                        }
                        return ListView.builder(
                            itemCount: productList.length,
                            shrinkWrap: true,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              // final item = productList[index];
                              return Salades(
                                  name: snapshot.data![index].name ?? 'Name',
                                  description:
                                      snapshot.data![index].description ??
                                          'Description',
                                  price: snapshot.data![index].originalPrice ??
                                      'Price',
                                  image: snapshot
                                          .data![index].defaultPhoto?.imgPath ??
                                      'Image',
                                  viewCount: snapshot.data![index].touchCount ??
                                      'Count',
                                  rateCount: snapshot.data![index].ratingDetails
                                          ?.totalRatingValue ??
                                      'Name',
                                  favorite: true);
                            });
                      }),
                ),
                const SizedBox(
                  height: 24,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      homeBoldText('home_forYou'.tr()),
                      Text(
                        'home_seeAll'.tr(),
                        style: TextStyle(
                            color: AppColors.unActive,
                            fontSize: 14,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                SizedBox(
                    height: 210,
                    child: FutureBuilder<List<Allproducts>>(
                        future: data,
                        builder: (context,
                            AsyncSnapshot<List<Allproducts>> snapshot) {
                          if (!snapshot.hasData) {
                            return const CircularProgressIndicator();
                          }
                          return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: productList.length,
                              itemBuilder: (context, index) {
                                return ForYouCard(
                                  name: snapshot.data![index].name ??
                                      'Default name',
                                  description:
                                      snapshot.data![index].description ??
                                          'Default description',
                                  price: snapshot.data![index].originalPrice ??
                                      'Default price',
                                  image: snapshot
                                          .data![index].defaultPhoto?.imgPath ??
                                      'Default Photo',
                                  viewCount: snapshot.data![index].touchCount ??
                                      'Default count',
                                  rateCount: snapshot.data![index].ratingDetails
                                          ?.totalRatingValue ??
                                      'Default name',
                                  favorite: true,
                                );
                              });
                        })),
                const SizedBox(
                  height: 24,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'home_special'.tr(),
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w900),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      FutureBuilder<List<Allproducts>>(
                          future: data,
                          builder: (context,
                              AsyncSnapshot<List<Allproducts>> snapshot) {
                            if (!snapshot.hasData) {
                              return const CircularProgressIndicator();
                            }
                            return ListView.builder(
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: productList.length,
                                itemBuilder: (context, index) {
                                  // Product item = productList[index];
                                  return SpecialCard(
                                      name: snapshot.data![index].name ??
                                          'default',
                                      description:
                                          snapshot.data![index].description ??
                                              'default description',
                                      price:
                                          snapshot.data![index].originalPrice ??
                                              '100000',
                                      image: snapshot.data![index].defaultPhoto
                                              ?.imgPath ??
                                          'assets/images/alcohol_img.png',
                                      viewCount:
                                          snapshot.data![index].touchCount ??
                                              '555',
                                      rateCount: snapshot
                                              .data![index]
                                              .ratingDetails
                                              ?.totalRatingValue ??
                                          '5',
                                      favorite: true);
                                });
                          }),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
        drawer: SizedBox(
          width: MediaQuery.of(context).size.width / 1.5,
          child: Drawer(
            backgroundColor: Colors.grey.withOpacity(0),
            elevation: 50,
            child: PopupMenu(
              userISLogined: true,
              userImage: "assets/Vector.png",
              userName: "John",
              userSurname: "Doe",
            ),
          ),
        ),
        drawerEnableOpenDragGesture: false,
      ),
    );
  }
}
