import 'package:flutter/material.dart';

import '../../../api_config/consturl.dart';
import '../../components/colors.dart';

class ForYouCard extends StatefulWidget {
  const ForYouCard({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite,
  }) : super(key: key);
  final String name;
  final String description;
  final String price;
  final String image;
  final String viewCount;
  final String rateCount;
  final bool favorite;

  @override
  State<ForYouCard> createState() => _ForYouCardState();
}

class _ForYouCardState extends State<ForYouCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      margin: const EdgeInsets.only(left: 16),
      width: 120,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: 120,
            height: 120,
            child: Image.network(AppUrl.subCategoryImageUrl + widget.image),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            widget.name,
            style: TextStyle(
                color: AppColors.mainColorDark,
                fontSize: 12,
                fontWeight: FontWeight.w500),
            maxLines: 2,
            overflow: TextOverflow.ellipsis,
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            widget.price,
            style: TextStyle(
                color: AppColors.mainColorLight,
                fontSize: 12,
                fontWeight: FontWeight.bold),
          ),
        ],
      ),
    );
  }
}
