// ignore: implementation_imports
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:vanfood/api_config/consturl.dart';

import '../../components/colors.dart';
import '../../components/space.dart';

class SpecialCard extends StatefulWidget {
  const SpecialCard({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite,
  }) : super(key: key);
  final String name;
  final String description;
  final String price;
  final String image;
  final String viewCount;
  final String rateCount;
  final bool favorite;
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<SpecialCard> {
  bool favorite = true;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      padding: const EdgeInsets.only(right: 5),
      margin: const EdgeInsets.symmetric(vertical: 5),
      height: Space.space93,
      decoration: BoxDecoration(
          color: AppColors.restorantBackColor,
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2,
              spreadRadius: 0.1,
              offset: Offset(0, 1),
            ),
          ],
          borderRadius: BorderRadius.circular(10)),
      child: FittedBox(
        fit: BoxFit.fill,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(AppUrl.subCategoryImageUrl +
              widget.image,
              width: Space.space93,
            ),
            ),
            const SizedBox(
              width: Space.space17,
            ),
            SizedBox(
              width: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.name,
                    style: TextStyle(
                      color: AppColors.mainColorDark,
                      fontSize: Space.space14,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: Space.space12,
                  ),
                  Text(
                    widget.description,
                    style: TextStyle(
                      color: AppColors.unActive,
                      fontSize: 10,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(widget.viewCount,
                    style: TextStyle(
                      color: AppColors.mainColorLight,
                      fontSize: Space.space12,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: Space.space28,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    setState(() {
                      favorite = !favorite;
                    });
                  },
                  child: favorite
                      ? Icon(
                          Icons.favorite_border_outlined,
                          color: AppColors.mainColorLight,
                        )
                      : Icon(
                          Icons.favorite,
                          color: AppColors.mainColorLight,
                        ),
                ),
                const SizedBox(
                  height: 51,
                ),
                Text(
                  widget.price,
                  style: TextStyle(
                    fontSize: 12,
                    color: AppColors.mainColorLight,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
