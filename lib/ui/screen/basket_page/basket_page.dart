import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../provider/current_user_provider.dart';
import '../../../repasitoris/current_user_service.dart';
import '../../components/back_button.dart';
import '../../components/colors.dart';
import '../../components/widgets.dart';
import '../home_page/home_page.dart';
import '../login_page/login_page.dart';
import '../purchase_check/purchase_check.dart';

class BasketPage extends StatelessWidget {
  const BasketPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CurrentUserProvider(),
      child: const Basket(),
    );
  }
}

class Basket extends StatefulWidget {
  const Basket({Key? key}) : super(key: key);

  @override
  State<Basket> createState() => _BasketState();
}

class _BasketState extends State<Basket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        titleSpacing: 0,
        backgroundColor: AppColors.light,
        title: BackToButton(
          backPageName: "navbar_main".tr(),
          press: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const HomePage(),
              ),
            );
          },
        ),
      ),
      body: Container(
        margin: const EdgeInsets.only(
          left: 16,
          right: 16,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Center(
                      child: Container(
                        margin: const EdgeInsets.only(
                          top: 8,
                        ),
                        child: SizedBox(
                          child: Text(
                            'navbar_busket'.tr(),
                            style: TextStyle(
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                              color: AppColors.mainColorLight,
                            ),
                          ),
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      child: ListView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: productList.length,
                          itemBuilder: (context, index) {
                            final item = productList[index];
                            return Dismissible(
                              key: UniqueKey(),
                              child: BasketProduct(
                                name: item.name,
                                description: item.description,
                                price: item.price,
                                image: item.image,
                                viewCount: item.viewCount,
                                rateCount: item.rateCount,
                                favorite: item.favorite,
                                selectedCount: item.selectedCount,
                                onTap: () {
                                  setState(() {
                                    productList.removeAt(index);
                                  });
                                },
                              ),
                              onDismissed: (_) {
                                setState(() {
                                  productList.removeAt(index);
                                });
                              },
                            );
                          }),
                    ),
                    widgetPrice('delivery'.tr(), '300 ' + 'price'.tr(), 12,
                        AppColors.unActive, FontWeight.normal),
                    widgetPrice('overall'.tr(), '2․600 ' + 'price'.tr(), 16,
                        AppColors.mainColorDark, FontWeight.bold),
                  ],
                ),
              ),
            ),
            SizedBox(
              child: Column(
                children: [
                  widgetBuyProduct(context, 'pay'.tr(), AppColors.light, 16,
                      AppColors.mainColorLight, FontWeight.bold, 0),
                  widgetBuyProduct(
                      context,
                      'continueShopping'.tr(),
                      AppColors.searchColor,
                      16,
                      AppColors.light,
                      FontWeight.normal,
                      15),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  widgetBuyProduct(context, String text, Color textColor, double textSize,
      Color contColor, FontWeight fontWeight, double contTop) {
    return GestureDetector(
      onTap: () {
        CurrentUserProvider model = Provider.of<CurrentUserProvider>(
          context,
          listen: false,
        );
        model.getCurrentUserState();
        CurrentUserService service = model.service;
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => service.hasCurrentUser
                ? const PurchaseCheckScreen()
                : LoginPage(),
          ),
        );
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: contColor,
        ),
        margin: EdgeInsets.only(
          top: contTop,
        ),
        padding: const EdgeInsets.only(
          top: 10,
          bottom: 10,
        ),
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width - 32,
        child: Text(
          text,
          style: TextStyle(
            fontSize: textSize,
            color: textColor,
            fontWeight: fontWeight,
          ),
        ),
      ),
    );
  }

  widgetPrice(String textLeft, String textRight, double textSize,
      Color textColor, FontWeight textWeight) {
    return Container(
      margin: const EdgeInsets.only(
        top: 15,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            child: Text(
              textLeft,
              style: TextStyle(
                color: textColor,
                fontSize: textSize,
                fontWeight: textWeight,
              ),
            ),
          ),
          SizedBox(
            child: Text(
              textRight,
              style: TextStyle(
                color: AppColors.unActive,
                fontSize: textSize,
                fontWeight: textWeight,
              ),
            ),
          ),
        ],
      ),
    );
  }

  sizedBoxWidget(String text, double sizeText, Color colorText,
      FontWeight theWeight, int maxLine) {
    return SizedBox(
      child: Text(
        text,
        maxLines: maxLine,
        style: TextStyle(
          fontSize: sizeText,
          color: colorText,
          fontWeight: theWeight,
        ),
      ),
    );
  }
}
