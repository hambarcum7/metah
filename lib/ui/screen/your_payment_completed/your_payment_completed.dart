import 'package:flutter/material.dart';
import 'package:vanfood/ui/screen/home_page/home_page.dart';

import '../../components/colors.dart';

class PaymentCompleted extends StatelessWidget {
  const PaymentCompleted({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final screenPhone = MediaQuery.of(context).size.width;
    double widthCenterImg = MediaQuery.of(context).size.width / 1.5;
    double widthLogo = MediaQuery.of(context).size.width / 2;
    if (screenPhone <= 325) {
      widthCenterImg = MediaQuery.of(context).size.width / 1.7;
      widthLogo = MediaQuery.of(context).size.width / 2.5;
    }
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
          padding: const EdgeInsets.only(
            left: 16,
            right: 16,
          ),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/paym_compl_background.png"),
                fit: BoxFit.cover),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                child: Column(
                  children: [
                    Image.asset(
                      'assets/logo.png',
                      width: widthLogo,
                    ),
                  ],
                ),
              ),
              widgetPrice('Ձեր վճարումը հաջողությամբ կատարվել է։', 15,
                  AppColors.mainColorLight, FontWeight.bold),
              SizedBox(
                child: Image.asset(
                  'assets/completed_page_center_foto.png',
                  width: widthCenterImg,
                ),
              ),
              widgetPrice('Շնորհակալություն գնումներ կատարելու համար։', 15,
                  AppColors.mainColorLight, FontWeight.bold),
              widgetBuyProduct(context, 'Շարունակել գնումները', AppColors.light,
                  16, AppColors.mainColorLight, FontWeight.bold, 0),
            ],
          ),
        ),
      ),
    );
  }

  widgetPrice(String textLeft, double textSize, Color textColor,
      FontWeight textWeight) {
    return FittedBox(
      child: SizedBox(
        child: Center(
          child: Text(
            textLeft,
            style: TextStyle(
              color: textColor,
              fontSize: textSize,
              fontWeight: textWeight,
            ),
          ),
        ),
      ),
    );
  }

  widgetBuyProduct(context, String text, Color textColor, double textSize,
      Color contColor, FontWeight fontWeight, double contTop) {
    return GestureDetector(
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: contColor,
        ),
        margin: EdgeInsets.only(
          top: contTop,
        ),
        padding: const EdgeInsets.only(
          top: 10,
          bottom: 10,
        ),
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width - 32,
        child: Text(
          text,
          style: TextStyle(
            fontSize: textSize,
            color: textColor,
            fontWeight: fontWeight,
          ),
        ),
      ),
    );
  }
}
