import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../../api_config/consturl_keys.dart';
import '../../../models/all_sub_category_model.dart';
import '../../../provider/provider_all_sub_categorys.dart';
import '../../../provider/provider_all_categorys.dart';
import '../../../repasitoris/all_cotegory_repo.dart';
import '../../components/back_button.dart';
import '../../components/search_input.dart';
import '../detail/detail.dart';
import '../home_page/home_page.dart';

class RestaurantsScreenState extends StatefulWidget {
  const RestaurantsScreenState({Key? key}) : super(key: key);

  @override
  State<RestaurantsScreenState> createState() => _RestaurantsScreenStateState();
}

class _RestaurantsScreenStateState extends State<RestaurantsScreenState> {
  List<Map<String, dynamic>> restaurantsData = [
    {
      "name": "rest_elkani".tr(),
      "workTime": "09:00 - 24:00",
      "time": "20 - 30",
      "star": "5.0",
      "image": "assets/images/elkani_restaurant.png",
      "menu": [
        {
          "title": "category_salades".tr(),
          "image": "assets/images/salad_img.png",
        },
        {
          "title": "food_fish".tr(),
          "image": "assets/images/fish_img.png",
        },
        {
          "title": "food_vegitables".tr(),
          "image": "assets/images/vegitable_img.png",
        },
        {
          "title": "food_drinks".tr(),
          "image": "assets/images/drink_img.png",
        },
        {
          "title": "food_alcohol".tr(),
          "image": "assets/images/alcohol_img.png",
        },
      ],
    },
    {
      "name": "madera".tr(),
      "workTime": "09:00 - 24:00",
      "time": "20 - 30",
      "star": "5.0",
      "image": "assets/images/madera_restaurant.png",
      "menu": [
        {
          "title": "category_salades".tr(),
          "image": "assets/images/salad_img.png",
        },
        {
          "title": "food_fish".tr(),
          "image": "assets/images/fish_img.png",
        },
        {
          "title": "food_vegitables".tr(),
          "image": "assets/images/vegitable_img.png",
        },
        {
          "title": "food_drinks".tr(),
          "image": "assets/images/drink_img.png",
        },
        {
          "title": "food_alcohol".tr(),
          "image": "assets/images/alcohol_img.png",
        },
      ],
    },
    {
      "name": "rest_ttujur".tr(),
      "workTime": "09:00 - 24:00",
      "time": "20 - 30",
      "star": "5.0",
      "image": "assets/images/ttujur_restaurant.png",
      "menu": [
        {
          "title": "category_salades".tr(),
          "image": "assets/images/salad_img.png",
        },
        {
          "title": "food_fish".tr(),
          "image": "assets/images/fish_img.png",
        },
        {
          "title": "food_vegitables".tr(),
          "image": "assets/images/vegitable_img.png",
        },
        {
          "title": "food_drinks".tr(),
          "image": "assets/images/drink_img.png",
        },
        {
          "title": "food_alcohol".tr(),
          "image": "assets/images/alcohol_img.png",
        },
      ],
    },
    {
      "name": "rest_gugark".tr(),
      "workTime": "09:00 - 24:00",
      "time": "20 - 30",
      "star": "5.0",
      "image": "assets/images/gugarq_restaurant.png",
      "menu": [
        {
          "title": "category_salades".tr(),
          "image": "assets/images/salad_img.png",
        },
        {
          "title": "food_fish".tr(),
          "image": "assets/images/fish_img.png",
        },
        {
          "title": "food_vegitables".tr(),
          "image": "assets/images/vegitable_img.png",
        },
        {
          "title": "food_drinks".tr(),
          "image": "assets/images/drink_img.png",
        },
        {
          "title": "food_alcohol".tr(),
          "image": "assets/images/alcohol_img.png",
        },
      ],
    },
  ];

  List<Map<String, dynamic>> filteredRestaurants = [];

  String searchText = "";

  TextEditingController searchTextController = TextEditingController();
  @override
  void initState() {
    super.initState();
    for (Map<String, dynamic> item in restaurantsData) {
      if (item["name"].contains(searchText)) {
        filteredRestaurants.add(item);
      }
    }
    response();
    super.initState();
  }

  void response() async {
    final model = VeiwModelAllSubCategorys();
    try {
      AppUrlKeys.cat_id = 'cataa6aa646bbf4c8bfe6c83d11095b41f0';
      List<dynamic> response = await model.getSpecialInfo();
      List allState = response.map((e) => AllSubCategory.fromJson(e)).toList();
      print('ressspppooooooooonse     ${allState[0].name}');
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Container(
            width: double.maxFinite - 30,
            height: double.maxFinite,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceH(25.0),
                // back button
                BackToButton(
                  backPageName: "Գլխավոր",
                  press: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const HomePage()));
                  },
                ),
                spaceH(18.0),
                // page title
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "Ռեստորաններ",
                      style: TextStyle(
                        color: Color.fromRGBO(18, 140, 126, 1),
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                spaceH(15.0),
                // search
                SearchInput(
                  controller: searchTextController,
                  press: () {
                    setState(() {
                      filteredRestaurants = [];
                      for (Map<String, dynamic> item in restaurantsData) {
                        if (item["name"].contains(searchTextController.text)) {
                          filteredRestaurants.add(item);
                        }
                      }
                    });
                  },
                ),
                spaceH(15.0),
                Expanded(
                  child: ListView.builder(
                      itemCount: filteredRestaurants.length,
                      itemBuilder: (BuildContext context, int i) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => Detail(),
                              ),
                            );
                          },
                          child: Container(
                            width: size.width - 30,
                            height: (size.width - 30) / 2.25,
                            child: Stack(
                              children: [
                                Container(
                                  width: size.width - 30,
                                  height: (size.width - 30) / 2.25,
                                  margin: const EdgeInsets.only(
                                    bottom: 8,
                                  ),
                                  decoration: BoxDecoration(
                                    color: Colors.red,
                                    borderRadius: BorderRadius.circular(8),
                                    image: DecorationImage(
                                      image: AssetImage(
                                          filteredRestaurants[i]["image"]),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),
                                Container(
                                  width: size.width - 30,
                                  height: (size.width - 30) / 2.25,
                                  margin: const EdgeInsets.only(
                                    bottom: 8,
                                  ),
                                  decoration: BoxDecoration(
                                    color: const Color.fromRGBO(0, 0, 0, 0.4),
                                    borderRadius: BorderRadius.circular(8),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      // star block
                                      Container(
                                        margin: const EdgeInsets.only(
                                          top: 15,
                                          left: 15,
                                        ),
                                        alignment: Alignment.center,
                                        width: 50,
                                        height: 20,
                                        decoration: BoxDecoration(
                                          color: const Color.fromRGBO(
                                              18, 140, 126, 1),
                                          borderRadius:
                                              BorderRadius.circular(2),
                                        ),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            // star icon
                                            const Icon(
                                              Icons.star,
                                              color: Colors.white,
                                              size: 12,
                                            ),
                                            spaceW(4.0),
                                            // star countՄադեռռա
                                            Text(
                                              filteredRestaurants[i]["star"],
                                              style: const TextStyle(
                                                color: Colors.white,
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const Spacer(),
                                      // restaurant name
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Text(
                                            filteredRestaurants[i]["name"],
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 18,
                                              fontWeight: FontWeight.w700,
                                            ),
                                          ),
                                        ],
                                      ),
                                      spaceH(5.0),
                                      // restaurant time info
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          restaurantTimeInfo(
                                              "ժամեր ${filteredRestaurants[i]["workTime"]}"),
                                          spaceW(15.0),
                                          restaurantTimeInfo(
                                              "${filteredRestaurants[i]["time"]} րոպե"),
                                        ],
                                      ),
                                      spaceH(15.0)
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget spaceW(size) {
    return SizedBox(
      width: size,
    );
  }

  Widget spaceH(size) {
    return SizedBox(
      height: size,
    );
  }

  Widget restaurantTimeInfo(text) {
    return Row(
      children: [
        Container(
          width: 5,
          height: 5,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(50),
          ),
        ),
        spaceW(8.0),
        Text(
          text,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 10,
            fontWeight: FontWeight.w600,
          ),
        )
      ],
    );
  }
}
