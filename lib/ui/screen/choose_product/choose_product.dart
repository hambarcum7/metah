import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../components/colors.dart';
import '../../components/widgets.dart';

class ChooseProduct extends StatefulWidget {
  const ChooseProduct({Key? key}) : super(key: key);

  @override
  State<ChooseProduct> createState() => _ChooseProductState();
}

class _ChooseProductState extends State<ChooseProduct> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: AppColors.light,
        title: Text(
          'selectedProducts'.tr(),
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold,
            color: AppColors.mainColorLight,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        margin: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
        child: Column(
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: productList.length,
                  itemBuilder: (context, index) {
                    final item = productList[index];
                    return FavoriteProduct(
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        image: item.image,
                        viewCount: item.viewCount,
                        rateCount: item.rateCount,
                        favorite: item.favorite);
                  }),
            ),
            Column(
              children: [
                Container(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: widgetPrice(
                  'overall'.tr(),
                  '3.300 ' + 'price'.tr(),
                  16,
                  AppColors.mainColorDark,
                  FontWeight.bold,
                ),
                ),
                widgetBuyProductChoose(
                    context,
                    'addToBasket'.tr(),
                    AppColors.light,
                    16,
                    AppColors.mainColorLight,
                    FontWeight.bold,
                    0,
                    'assets/product_choose_butt_icon.png'),
                widgetBuyProduct(
                    context,
                    'continueShopping'.tr(),
                    AppColors.searchColor,
                    16,
                    AppColors.light,
                    FontWeight.normal,
                    15),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
