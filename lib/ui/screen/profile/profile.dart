// ignore: implementation_imports
import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/provider/login_view_model.dart';
import '../login_page/login_page.dart';
import '../../components/colors.dart';
import '../../components/widgets.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => LoginViewModel(),
      child: ProfilePageState(),);
  }
}

class ProfilePageState extends StatefulWidget {
  const ProfilePageState({Key? key}) : super(key: key);

  @override
  State<ProfilePageState> createState() => _ProfilePageStateState();
}

class _ProfilePageStateState extends State<ProfilePageState> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      locale: context.locale,
      home: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            titleSpacing: 2,
            elevation: 0,
            backgroundColor: AppColors.mainColorLight,
          ),
          body: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(
                  width: double.maxFinite,
                  height: 200,
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          Container(
                            width: double.maxFinite,
                            height: 100,
                            decoration: BoxDecoration(
                              color: AppColors.mainColorLight,
                              borderRadius: const BorderRadius.only(
                                bottomLeft: Radius.circular(24),
                                bottomRight: Radius.circular(24),
                              ),
                            ),
                          ),
                          Container(
                            width: double.maxFinite,
                            height: 100,
                            decoration: const BoxDecoration(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                      Center(
                        child: Column(
                          children: [
                            const Spacer(),
                            profileText('profile_myProfile'.tr(), Colors.white),
                            const SizedBox(
                              height: 15,
                            ),
                            profileImage(),
                            const SizedBox(
                              height: 15,
                            ),
                            profileText('Ani Khazaryan',
                                AppColors.mainColorDark),
                            const Spacer(),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  width: double.maxFinite,
                  padding:
                      const EdgeInsets.symmetric(vertical: 31, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      profileItem('profile_myPayments'.tr(),(){}),
                      profileItem('profile_myCards'.tr(),(){}),
                      profileItem('profile_myAdrees'.tr(),(){}),
                      profileItem('profile_privacy'.tr(),(){}),
                      profileItem('profile_FAQ'.tr(),(){}),
                      profileItem('profile_logout'.tr(),(){
                        context.read<LoginViewModel>().logOut();
                        Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
                      }),
                    ],
                  ),
                ),
              ],
            ),
          )),
    );
  }
}
