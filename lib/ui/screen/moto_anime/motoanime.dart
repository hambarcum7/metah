

import 'package:flutter/material.dart';
import 'package:vanfood/ui/screen/detail/detail.dart';

import '../../components/colors.dart';
import '../on_bording/onboarding_page_template.dart';





class MotoAnime extends StatefulWidget {
  const MotoAnime({Key? key}) : super(key: key);

  @override
  State<MotoAnime> createState() => _MotoAnime();
}

class _MotoAnime extends State<MotoAnime> with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 3),
    vsync: this,
  );

  late Animation<double> animation;
  @override
  void initState() {
    animation = Tween<double>(begin: 0.0, end: 1000).animate(_controller)..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const Detail(),
              ));
        }
      });
    _controller.forward();
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        body: Stack(
          children: [
            SizedBox(
              width: double.maxFinite,
              height: double.maxFinite,
              child: Image.asset(
                'assets/img/fon.png',
                fit: BoxFit.cover,
              ),
            ),
            SizedBox(
              width: double.maxFinite,
              height: double.maxFinite,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AnimatedBuilder(
                    animation: _controller,
                    child: SizedBox(
                      child: Image.asset('assets/img/animation.png'),
                    ),
                    builder: (BuildContext context, Widget? child) {
                      return Transform.translate(
                        offset: Offset(animation.value, 0.0),
                        child: child,
                      );
                    },
                  ),
                  AnimatedBuilder(
                    animation: _controller,
                    child: Column(
                      children: [
                        Text(
                          'VANFOOD',
                          style: TextStyle(
                            color: AppColors.vanFood,
                            fontSize: 55,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'DELIVERY COMPANY',
                          style: TextStyle(
                            color: AppColors.mainColorDark,
                            fontSize: 25,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    builder: (BuildContext context, Widget? child) {
                      return Transform.translate(
                        offset: Offset(-animation.value, 0.0),
                        child: child,
                      );
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}