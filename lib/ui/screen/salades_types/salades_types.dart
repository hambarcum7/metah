import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../components/colors.dart';
import '../product_detail/product_detail.dart';

class MySaladeTips extends StatefulWidget {
   MySaladeTips({
    Key? key,
    required this.name
  }) : super(key: key);
  String name;
  @override
  _MySaladeTips createState() => _MySaladeTips();
}

class _MySaladeTips extends State<MySaladeTips> {
  int number = 0;
  bool favorit = true;
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: AppColors.restorantBackColor,
      ),
      padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 8),
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 8),
      child: Row(
        children: [
          Image.asset('assets/img/GugoiNkar.png'),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Text(
                        widget.name,
                        maxLines:2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontSize: 15, color: AppColors.mainColorLight),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        setState(() {
                          favorit = !favorit;
                        });
                      },
                      child: favorit
                          ? Icon(
                              Icons.favorite_border_outlined,
                              color: AppColors.mainColorLight,
                            )
                          : Icon(
                              Icons.favorite,
                              color: AppColors.mainColorLight,
                            ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 8,
                ),
                SizedBox(
                  width: width / 2,
                  child: Text(
                    'Լորեմ Իպսումը տպագրության և տպագրական արդյունաբերության...',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(fontSize: 12, color: AppColors.unActive),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  '1.800',
                  style: TextStyle(
                    color: AppColors.mainColorLight,
                  ),
                ),
                const SizedBox(
                  height: 12,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: 70,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          counter(() {
                            setState(() {
                              if (number > 0) {
                                number--;
                              }
                            });
                          }, '-', Colors.black),
                          Text(
                            '$number',
                            style:
                                TextStyle(color: AppColors.dark, fontSize: 17),
                          ),
                          counter(() {
                            setState(() {
                              number++;
                            });
                          }, '+', AppColors.mainColorLight)
                        ],
                      ),
                    ),
                    Spacer(),
                    Expanded(
                      flex: 8,
                      child: GestureDetector(
                        onTap:() => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ProductDetail())),
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                              vertical: 6,),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: AppColors.mainColorLight,
                          ),
                          width: 100,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Text(
                                'add'.tr(),
                                style: TextStyle(color: AppColors.light),
                              ),
                              Icon(Icons.add_shopping_cart, color: AppColors.light)
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

counter(param0, String s, Color color) {
  return GestureDetector(
      onTap: param0,
      child: Container(
        width: 24,
        height: 24,
        child: Center(child: Text(s,style: TextStyle(fontSize: 20,color: color),)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(
              4,
            ),
            color: Colors.white),
      ));
}
