import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/repasitoris/all_sub_category_repo.dart';
import '../../../models/all_sub_category_model.dart';
import '../../../provider/provider_all_sub_categorys.dart';
import '../../components/back_button.dart';
import '../../components/colors.dart';
import '../../components/widgets.dart';
import '../salades_types/salades_types.dart';

class SaladePage extends StatefulWidget {
  const SaladePage({Key? key}) : super(key: key);

  @override
  SaladeProvider createState() => SaladeProvider();
}

class SaladeProvider extends State<SaladePage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => VeiwModelAllSubCategorys(),  
      child: _SaladePageState(),
    );
  }

  
}

class _SaladePageState extends StatefulWidget {
  @override
  State<_SaladePageState> createState() => _SaladePageStateState();
}

class _SaladePageStateState extends State<_SaladePageState> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: AppColors.mainColorLight,
          elevation: 0,
          title: BackToButton(
            backPageName: "Գլխավոր",
            press: () {
              Navigator.pop(context);
            },
          ),
          actions: [
            Container(
              margin: const EdgeInsets.only(right: 10),
              child: const Icon(Icons.search),
            )
          ],
        ),
        body: SizedBox(
          height: double.maxFinite,
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.symmetric(vertical: 15),
                width: double.maxFinite,
                decoration: BoxDecoration(
                    border: null,
                    borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(24),
                      bottomRight: Radius.circular(24),
                    ),
                    color: AppColors.mainColorLight),
                child: Center(
                    child: Text(
                  'category_salades'.tr(),
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.w700),
                )),
              ),
              Expanded(
                child: Container(
                  color: Colors.white,
                  child: ListView.builder(
                      itemCount: productList.length,
                      itemBuilder: (context, index) {
                        var item = productList[index];
                        return MySaladeTips(
                          name: item.name,
                        );
                      }),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
