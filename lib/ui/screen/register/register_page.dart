import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/repasitoris/login_service.dart';
import 'package:vanfood/provider/login_view_model.dart';
import 'package:vanfood/ui/screen/confirm_sms/confirm_screen.dart';
import 'package:vanfood/ui/screen/home_page/home_page.dart';
import 'package:vanfood/ui/screen/profile/profile.dart';

import '../../components/back_button.dart';
import '../../components/colors.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => LoginViewModel(),
      child: RegisterPageState(),
    );
  }
}

String name = '';
String surname = '';

class RegisterPageState extends StatefulWidget {
  @override
  State<RegisterPageState> createState() => _RegisterPageStateState();
}

class _RegisterPageStateState extends State<RegisterPageState> {
final TextEditingController loginTextController = TextEditingController();

  final TextEditingController passwordTextController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  Color activeColor = Colors.white;

  Color disableColor = const Color(0xffF4F6F9);

  bool signInWithEmail = true;

  bool signInWithPhone = false;

  double marginTopItems = 10;

  String firebaseErrorText = '';

  @override
  Widget build(BuildContext context) {
    LoginService model = context.watch<LoginViewModel>().service;
    double a = MediaQuery.of(context).size.height;
    dynamic scrool = Axis.horizontal;
    if (a < 550) {
      scrool = Axis.vertical;
    }
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          elevation: 0,
          titleSpacing: 0,
          backgroundColor: AppColors.light,
          title: Row(
            children: [
              BackToButton(
                backPageName: 'navbar_main'.tr(),
                press: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const HomePage())),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          scrollDirection: scrool,
          child: Container(
            color: AppColors.light,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              //mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(
                    left: 13,
                    bottom: 10,
                  ),
                  decoration: const BoxDecoration(
                      border: Border(
                    bottom: BorderSide(
                      width: 1,
                      color: Color(0xffB3B3B3),
                    ),
                  )),
                  child: Text(
                    'login'.tr(),
                    style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ),
                // button email and tel numb
                emailAndTelephone(),
                //form
                Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        top: marginTopItems,
                      ),
                      width: MediaQuery.of(context).size.width - 38,
                      child: Form(
                        key: _formKey,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              firebaseErrorText,
                              style: const TextStyle(color: Colors.red),
                            ),
                            spaceH(8.0),
                            // if selected signin with email show two input else one
                            signInWithEmail
                                ? Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      textInput(
                                        'enter_E-mail'.tr(),
                                        loginTextController,
                                        false,
                                        model,
                                      ),
                                      spaceH(10.0),
                                      textInput(
                                        'Password',
                                        passwordTextController,
                                        true,
                                        model,
                                      ),
                                    ],
                                  )
                                : textInput(
                                    'enter_telNumber'.tr(),
                                    loginTextController,
                                    false,
                                    model,
                                  ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: marginTopItems,
                      ),
                      width: MediaQuery.of(context).size.width - 38,
                      child: RichText(
                        softWrap: true,
                        text: TextSpan(
                          text: 'Շարունակելով՝ դուք համաձայնում եք ',
                          style: TextStyle(
                            fontSize: 12,
                            color: AppColors.dark,
                          ),
                          children: <TextSpan>[
                            addTextSpan('ծառայության պայմաններին ', 10.0),
                            const TextSpan(text: 'և '),
                            addTextSpan('Գաղտնիության քաղաքականությանը ', 10.0),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
                // button next
                buttonSample(
                  'continue'.tr(),
                  AppColors.mainColorLight,
                  '',
                  AppColors.light,
                  () async {
                    if (_formKey.currentState != null &&
                        _formKey.currentState!.validate()) {
                      // if selected signin with email
                      if (signInWithEmail) {
                        await context.read<LoginViewModel>().signUpWithEmail(
                              loginTextController.text,
                              passwordTextController.text,
                            );
                        setState(() {
                          firebaseErrorText = model.errorText;
                        });
                        if (!model.hasError) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const HomePage(),
                            ),
                          );
                        }
                      } else {
                        // if selected sign in with phone
                        await context.read<LoginViewModel>().verifyNumber(
                              loginTextController.text,
                            );
                        setState(() {
                          firebaseErrorText = model.errorText;
                        });
                        if (!model.hasError) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const ConfirmScreen(),
                            ),
                          );
                        }
                      }
                    }
                  },
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.only(
                      top: marginTopItems,
                    ),
                    child: Text(
                      'or'.tr(),
                      style: const TextStyle(
                        fontSize: 12,
                      ),
                    ),
                  ),
                ),
                // sign in with apple,facebook,google buttons
                // SizedBox(
                //   child: Column(
                //     children: [
                //       buttonSample(
                //         'Continue with Apple',
                //         AppColors.colorsSocialNetworks,
                //         'assets/apple.png',
                //         AppColors.mainColorLight,
                //         () {},
                //       ),
                //       buttonSample(
                //         'Continue with Facebook',
                //         AppColors.colorsSocialNetworks,
                //         'assets/facebook.png',
                //         AppColors.mainColorLight,
                //         () {},
                //       ),
                //       buttonSample(
                //         'Continue with Google',
                //         AppColors.colorsSocialNetworks,
                //         'assets/gmail.png',
                //         AppColors.mainColorLight,
                //         () {},
                //       ),
                //     ],
                //   ),
                // ),
                // sign in as guest
                Container(
                  margin: const EdgeInsets.only(
                    top: 8,
                    bottom: 8,
                  ),
                  child: FittedBox(
                    child: Text(
                      'asGuest'.tr(),
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.mainColorLight,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  TextSpan addTextSpan(String text, double fs) {
    return TextSpan(
      text: text,
      style: TextStyle(
        fontSize: fs,
        color: AppColors.mainColorLight,
      ),
    );
  }

  // Widget button email and telephone number
  Widget emailAndTelephone() {
    return Container(
      padding: const EdgeInsets.all(1),
      margin: EdgeInsets.only(
        top: marginTopItems,
      ),
      width: MediaQuery.of(context).size.width - 38,
      decoration: BoxDecoration(
        color: const Color(0xffF4F6F9),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          // button email
          signInCategoryButton(
            () {
              setState(() {
                signInWithEmail = true;
                signInWithPhone = false;
              });
            },
            'e-mail'.tr(),
            signInWithEmail,
          ),
          // button phone
          signInCategoryButton(
            () {
              setState(() {
                signInWithPhone = true;
                signInWithEmail = false;
              });
            },
            'telNumber'.tr(),
            signInWithPhone,
          ),
        ],
      ),
    );
  }

  // sign in category buton (email or phone)
  Widget signInCategoryButton(callback, String buttonText, isActive) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width / 2 - 20,
        padding: const EdgeInsets.all(6),
        decoration: BoxDecoration(
          color: isActive ? activeColor : disableColor,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Text(buttonText),
      ),
    );
  }

  // text form field widget
  Widget textInput(
    String hintText,
    controller,
    bool isPassword,
    LoginService model,
  ) {
    return TextFormField(
      controller: controller,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: const TextStyle(
          fontSize: 15,
        ),
        contentPadding: const EdgeInsets.symmetric(
          vertical: 0,
          horizontal: 10,
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Color(0xffDADEE3),
            width: 1,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
          borderSide: const BorderSide(
            color: Color(0xffDADEE3),
            width: 1,
          ),
        ),
      ),
      obscureText: isPassword,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return "Please enter info";
        } else if (model.hasError) {
          return model.errorText;
        } else if (isPassword && value.length < 6) {
          return "Password should be longer than 5 character";
        } else if (!isPassword && !emailValid(value) && signInWithEmail) {
          return "Please enter the correct email";
        }
        return null;
      },
    );
  }

  bool emailValid(String text) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(text);
  }

  Widget spaceH(double size) {
    return SizedBox(
      height: size,
    );
  }

  Widget spaceW(double size) {
    return SizedBox(
      width: size,
    );
  }

  // Button sample
  Widget buttonSample(
      String text, Color background, dynamic img, Color colorText, callback) {
    return GestureDetector(
      onTap: callback,
      child: Container(
        padding: const EdgeInsets.all(8),
        margin: EdgeInsets.only(
          top: marginTopItems,
        ),
        width: MediaQuery.of(context).size.width - 38,
        decoration: BoxDecoration(
          color: background,
          borderRadius: BorderRadius.circular(20),
        ),
        child: img != ''
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    img,
                    width: 19,
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                      left: 8,
                    ),
                    child: FittedBox(
                      alignment: Alignment.center,
                      child: Text(
                        text,
                        style: TextStyle(
                          fontSize: 16,
                          color: colorText,
                        ),
                      ),
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    child: FittedBox(
                        alignment: Alignment.center,
                        child: Text(
                          text,
                          style: TextStyle(
                            fontSize: 16,
                            color: colorText,
                          ),
                        )),
                  ),
                ],
              ),
      ),
    );
  }
}
