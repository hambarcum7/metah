import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class PopupMenu extends StatefulWidget {
  PopupMenu({
    Key? key,
    required this.userISLogined,
    this.userImage = "",
    this.userName = "",
    this.userSurname = "",
  }) : super(key: key);

  bool userISLogined;
  String userImage;
  String userName;
  String userSurname;

  @override
  _PopupMenuState createState() => _PopupMenuState();
}

class _PopupMenuState extends State<PopupMenu> {
  String iconsFolderPath = "../../assets/icons_menu/";
  List<Map<String, dynamic>> menuItemsInfo = [
    {
      "icon": "salad_icon.png",
      "text": "category_salades".tr(),
    },
    {
      "icon": "pizza_icon.png",
      "text": "category_pizza".tr(),
    },
    {
      "icon": "drink_icon.png",
      "text": "category_drink".tr(),
    },
    {
      "icon": "sweet_icon.png",
      "text": "category_sweets".tr(),
    },
    {
      "icon": "coffee_icon.png",
      "text": "category_coffee".tr(),
    },
    {
      "icon": "shaurma_icon.png",
      "text": "category_shaurma".tr(),
    },
    {
      "icon": "seeAll_icon.png",
      "text": "home_seeAll".tr(),
    },
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      // popup-menu parent
      width: size.width * 0.6,
     
      decoration: const BoxDecoration(
        color: Color.fromRGBO(18, 140, 126, 1),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(24),
          bottomRight: Radius.circular(24),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          spaceH(20),
          // close button
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                alignment: Alignment.center,
                width: 30,
                height: 30,
                child: const Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          spaceH(20),
          // user info
          widget.userISLogined
              ? Container(
                  alignment: Alignment.center,
                  width: double.maxFinite,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      // avatar
                      ClipOval(
                        child: SizedBox.fromSize(
                          size: Size.fromRadius((size.width * 0.6)/4), // Image radius
                          child:
                              Image.asset(widget.userImage, fit: BoxFit.cover),
                        ),
                      ),
                      spaceH(15),
                      // name & surname
                      SizedBox(
                        width: (size.width * 0.6) / 2.35,
                        child: Text(
                          "${widget.userName}\n${widget.userSurname}",
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                          maxLines: 2,
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
          spaceH(15),
          // line
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            width: (size.width * 0.6) - 40,
            height: 1,
            color: Colors.white.withOpacity(0.3),
          ),
          spaceH(15),
          // menu items
          Expanded(
            child: ListView.builder(
              itemCount: 7,
              itemBuilder: (BuildContext context, int i) {
                return GestureDetector(
                  onTap: () {},
                  child: SizedBox(
                    width: size.width * 0.6,
                    height: 50,
                    child: Row(
                      children: [
                        spaceW(20),
                        ImageIcon(
                          AssetImage(
                            iconsFolderPath + menuItemsInfo[i]["icon"],
                          ),
                          color: const Color.fromRGBO(131, 195, 187, 1),
                        ),
                        spaceW(20),
                        Text(
                          menuItemsInfo[i]["text"],
                          style: const TextStyle(
                            color: Color.fromRGBO(131, 195, 187, 1),
                            fontSize: 15,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          spaceH(25)
        ],
      ),
    );
  }

  Widget spaceH(double size) {
    return SizedBox(
      height: size,
    );
  }

  Widget spaceW(double size) {
    return SizedBox(
      width: size,
    );
  }
}
