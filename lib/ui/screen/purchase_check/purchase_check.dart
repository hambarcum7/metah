import 'package:flutter/material.dart';
import 'package:vanfood/ui/mycircle.dart';

import '../../components/back_button.dart';

class PurchaseCheckScreen extends StatefulWidget {
  const PurchaseCheckScreen({Key? key}) : super(key: key);

  @override
  _PurchaseCheckScreenState createState() => _PurchaseCheckScreenState();
}

class _PurchaseCheckScreenState extends State<PurchaseCheckScreen> {
  List<Map<String, dynamic>> shippingMethods = [
    {
      "icon": "assets/people.png",
      "method_name": "Ոտքով",
      "soon": false,
    },
    {
      "icon": "assets/bike.png",
      "method_name": "Հեծանիվով",
      "soon": false,
    },
    {
      "icon": "assets/car.png",
      "method_name": "Մեքենայով",
      "soon": false,
    },
    {
      "icon": "assets/dron.png",
      "method_name": "Դռոնով",
      "soon": true,
    },
  ];

  int selectedIndex = 0; // for shipping method

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SizedBox(
          width: double.maxFinite,
          height: double.maxFinite,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              spaceH(25.0),
              // back button
              BackToButton(
                backPageName: "Զամբյուղ",
                press: () {
                  Navigator.pop(context);
                },
              ),
              spaceH(20.0),
              Expanded(
                child: Container(
                  width: double.maxFinite - 30,
                  margin: const EdgeInsets.symmetric(horizontal: 15),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Text(
                          "Ստուգում",
                          style: TextStyle(
                            color: Color.fromRGBO(18, 140, 126, 1),
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        spaceH(15.0),
                        orderInfoBlock(
                          size,
                          "Վճարման կարգ",
                          'assets/card.png',
                          "**** **** **** 4747",
                          () {},
                        ),
                        spaceH(25.0),
                        orderInfoBlock(
                          size,
                          "Առաքման հասցե",
                          'assets/home_icon.png',
                          "Երևանյան խճուղի 145",
                          () {},
                        ),
                        spaceH(25.0),
                        // Shipping method title
                        const Text(
                          "Առաքման միջոցը",
                          style: TextStyle(
                            color: Color.fromRGBO(4, 64, 57, 1),
                            fontSize: 18,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        spaceH(15.0),
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: List.generate(
                              shippingMethods.length,
                              (i) => GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        if (!shippingMethods[i]["soon"]) {
                                          selectedIndex = i;
                                        }
                                      });
                                    },
                                    child: Container(
                                      width: double.maxFinite,
                                      height: 30,
                                      decoration: BoxDecoration(
                                        color: selectedIndex == i
                                            ? const Color.fromRGBO(
                                                243, 249, 248, 1)
                                            : Colors.white,
                                      ),
                                      child: Row(
                                        children: [
                                          // icon
                                          if (shippingMethods[i]["soon"])
                                            shippingMethodsIconColor(
                                              const Color.fromRGBO(
                                                  131, 195, 187, 1),
                                              i,
                                            )
                                          else if (selectedIndex == i)
                                            shippingMethodsIconColor(
                                              const Color.fromRGBO(
                                                  18, 140, 126, 1),
                                              i,
                                            )
                                          else
                                            shippingMethodsIconColor(
                                              const Color.fromRGBO(
                                                  45, 12, 87, 1),
                                              i,
                                            ),
                                          spaceW(25.0),
                                          // text
                                          if (shippingMethods[i]["soon"])
                                            shippingMethodsTextColor(
                                              const Color.fromRGBO(
                                                  131, 195, 187, 1),
                                              i,
                                            )
                                          else if (selectedIndex == i)
                                            shippingMethodsTextColor(
                                              const Color.fromRGBO(
                                                  18, 140, 126, 1),
                                              i,
                                            )
                                          else
                                            shippingMethodsTextColor(
                                              Colors.black,
                                              i,
                                            ),
                                          spaceW(20.0),
                                          shippingMethods[i]["soon"]
                                              ? Container(
                                                  width: 73,
                                                  height: 16,
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    color: const Color.fromRGBO(
                                                        131, 195, 187, 1),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            2),
                                                  ),
                                                  child: const Text(
                                                    "Շուտով",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 10,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                    ),
                                                  ),
                                                )
                                              : Container(),
                                          const Spacer(),
                                          selectedIndex == i
                                              ? Icon(
                                                  Icons.check,
                                                  color: selectedIndex == i
                                                      ? const Color.fromRGBO(
                                                          18, 140, 126, 1)
                                                      : Colors.black,
                                                )
                                              : Container(),
                                          spaceW(5.0),
                                        ],
                                      ),
                                    ),
                                  )),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              spaceH(25.0),
              GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const MyAnime(),
                    ),
                  );
                },
                child: Container(
                  width: double.maxFinite - 30,
                  margin: const EdgeInsets.symmetric(horizontal: 15),
                  height: 40,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: const Color.fromRGBO(18, 140, 126, 1),
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: const Text(
                    "Հաստատել",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 16,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
              spaceH(25.0),
            ],
          ),
        ),
      ),
    );
  }

  Widget shippingMethodsTextColor(Color color, i) {
    return Text(
      shippingMethods[i]["method_name"],
      style: TextStyle(
        fontSize: 16,
        fontWeight: FontWeight.w500,
        color: color,
      ),
    );
  }

  Widget shippingMethodsIconColor(Color color, i) {
    return ImageIcon(
      AssetImage(shippingMethods[i]["icon"]),
      color: color,
    );
  }

  Widget spaceH(h) {
    return SizedBox(
      height: h,
    );
  }

  Widget spaceW(w) {
    return SizedBox(
      width: w,
    );
  }

  Widget orderInfoBlock(size, title, icon, text, change) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 3,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // title
              Text(
                title,
                style: const TextStyle(
                  color: Color.fromRGBO(4, 64, 57, 1),
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                ),
              ),
              spaceH(10.0),
              // icon & text
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // icon
                  ImageIcon(
                    AssetImage(icon),
                    color: const Color.fromRGBO(18, 140, 126, 1),
                  ),
                  spaceW(15.0),
                  // text
                  SizedBox(
                    width: size.width * 0.5,
                    child: Text(
                      text,
                      style: const TextStyle(
                        color: Color.fromRGBO(131, 195, 187, 1),
                        fontSize: 17,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            alignment: Alignment.topRight,
            child: GestureDetector(
              onTap: change,
              child: const Text(
                "Փոխել",
                style: TextStyle(
                  color: Color.fromRGBO(131, 195, 187, 1),
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
