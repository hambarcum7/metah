import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/repasitoris/current_user_service.dart';
import 'package:vanfood/provider/current_user_provider.dart';
import 'package:vanfood/ui/components/responsive_button.dart';
import 'package:vanfood/ui/screen/login_page/login_page.dart';

import '../home_page/home_page.dart';
import 'package:easy_localization/easy_localization.dart';

class Onboarding extends StatelessWidget {
  const Onboarding({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CurrentUserProvider(),
      child: const OnboardingPage(),
    );
  }
}

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({
    Key? key,
  }) : super(key: key);

  @override
  State<OnboardingPage> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  int indecatorIndex = 0;

  List<Map<String, dynamic>> onboardingPageInfo = [
    {
      "image": "assets/images/page_one_vector_img.png",
      "title": "welcome".tr(),
      "text": "vanfoodWelcomes".tr(),
    },
    {
      "image": "assets/images/page_two_vector_img.png",
      "text": "vanadzorDelivery".tr(),
    },
    {
      "image": "assets/images/page_three_vector_img.png",
      "text": "speedWork".tr(),
    }
  ];

  PageController pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: PageView.builder(
            itemCount: onboardingPageInfo.length,
            controller: pageController,
            itemBuilder: (BuildContext context, int i) {
              return SizedBox(
                width: double.maxFinite,
                height: double.maxFinite,
                child: Stack(
                  children: [
                    Image.asset(
                      "assets/images/background.png",
                      fit: BoxFit.fill,
                      height: double.maxFinite,
                      width: double.maxFinite,
                    ),
                    SafeArea(
                      child: SizedBox(
                        width: double.maxFinite,
                        height: double.maxFinite,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(
                              height: 25,
                            ),
                            Expanded(
                              child:
                                  Image.asset(onboardingPageInfo[i]["image"]),
                            ),
                            i == 0
                                ? Text(
                                    "welcome".tr(),
                                    style: const TextStyle(
                                      fontSize: 24,
                                      color: Color.fromRGBO(4, 64, 57, 1),
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )
                                : Container(),
                            const SizedBox(
                              height: 15,
                            ),
                            Container(
                              width: double.maxFinite - 60,
                              margin:
                                  const EdgeInsets.symmetric(horizontal: 30),
                              child: Text(
                                onboardingPageInfo[i]["text"],
                                style: const TextStyle(
                                  fontSize: 20,
                                  color: Color.fromRGBO(4, 64, 57, 1),
                                  fontWeight: FontWeight.w500,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            const SizedBox(
                              height: 25,
                            ),
                            // indecators
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: List.generate(
                                3,
                                (index) => Container(
                                  width: index == i ? 25 : 8,
                                  height: 8,
                                  margin: const EdgeInsets.only(right: 5),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(16),
                                    color: index == i
                                        ? const Color.fromRGBO(
                                            18, 140, 126, 1)
                                        : const Color.fromRGBO(
                                            126, 197, 191, 1),
                                  ),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 30,
                            ),
                            ResponsiveButton(
                              isFilled: true,
                              callback: () {
                                if (i < onboardingPageInfo.length - 1) {
                                  pageController.jumpToPage(++i);
                                  setState(() {
                                    i++;
                                  });
                                } else {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => const HomePage()
                                    ),
                                  );
                                }
                              },
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                            ResponsiveButton(
                              isFilled: false,
                              callback: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => const HomePage(),
                                    ),
                                  );
                              },
                            ),
                            const SizedBox(
                              height: 15,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }),
      ),
    );
  }
}
