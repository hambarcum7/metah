import 'package:flutter/material.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/ui/components/colors.dart';
import 'package:vanfood/ui/screen/home_page/home_page.dart';

import '../../../provider/login_view_model.dart';

class ConfirmScreen extends StatelessWidget {
  const ConfirmScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => LoginViewModel(),
      child: ConfirmPage(),
    );
  }
}

class ConfirmPage extends StatefulWidget {
  const ConfirmPage({Key? key}) : super(key: key);

  @override
  State<ConfirmPage> createState() => _ConfirmPageState();
}

class _ConfirmPageState extends State<ConfirmPage> {
  TextEditingController pinController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.mainColorLight,
      ),
      body: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        child: Column(
          children: [
            spaceH(25.0),
            const Text(
              "Verification",
              style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700),
            ),
            spaceH(15.0),
            const Text(
              "Please enter a PIN to continue.",
              style: TextStyle(
                color: Color.fromARGB(255, 134, 127, 127),
                fontSize: 18,
              ),
            ),
            spaceH(20.0),
            Text(Provider.of<LoginViewModel>(context).service.errorText),
            spaceH(15.0),
            Pinput(
              length: 6,
              controller: pinController,
              onCompleted: (String value) async {
                LoginViewModel model = Provider.of<LoginViewModel>(context,listen: false);
                await model.signinWithTelephone(value);
                if (model.service.hasError) {
                  pinController.clear();
                } else {
                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const HomePage(),
                    ),
                    (route) => false,
                  );
                }
              },
              closeKeyboardWhenCompleted: true,
            ),
          ],
        ),
      ),
    );
  }

  Widget spaceH(double h) {
    return SizedBox(
      height: h,
    );
  }

  Widget spaceW(double w) {
    return SizedBox(
      width: w,
    );
  }
}
