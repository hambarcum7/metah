import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vanfood/repasitoris/all_products_repo.dart';
import 'package:vanfood/ui/screen/product_detail/product_detail_item.dart';
import 'package:vanfood/ui/screen/special/special.dart';

import '../../../models/all_products/all_products.dart';
import '../../components/back_button.dart';
import '../../components/colors.dart';
import '../../components/space.dart';
import '../../components/widgets.dart';
import '../card/foryou_card.dart';

class ProductDetail extends StatefulWidget {
  const ProductDetail({Key? key}) : super(key: key);

  @override
  State<ProductDetail> createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  bool IsAnimatedImageTime = false;
  int productCount = 1;
  Map<String, dynamic> productData = {
    "name": "food_tavar".tr(),
    "image": "assets/products.png",
    "description":
        "Լորեմ Իպսումը տպագրության և տպագրական արդյունաբերության համար նախատեսված մոդելային տեքստ է",
    "shipping_info": {
      "productWeight": "250 " + "g".tr(),
      "shippingTime": "15 " + "minute".tr(),
    },
    "stars": 5.0,
    "price": 800,
  };
  double x = 0;
  double y = -0.65;
  double imgh = 250;
  double imgw = 250;
  bool favorite = true;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return MaterialApp(
      debugShowCheckedModeBanner: true,
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: SizedBox(
            width: double.maxFinite,
            height: double.maxFinite,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: double.maxFinite,
                        decoration: const BoxDecoration(
                          color: Color.fromRGBO(243, 249, 248, 1),
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            spaceH(25.0),
                            // back button
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                spaceW(15.0),
                                BackToButton(
                                  backPageName: "category_salades".tr(),
                                  press: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ],
                            ),
                            spaceH(17.0),
                            // title
                            Center(
                              child: Text(
                                productData["name"],
                                style: const TextStyle(
                                  color: Color.fromRGBO(18, 140, 126, 1),
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      // product image part
                      SizedBox(
                        width: double.maxFinite,
                        height: size.width * 0.6,
                        child: Stack(
                          alignment: Alignment.center,
                          children: [
                            // background of image block
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Container(
                                    width: double.maxFinite,
                                    decoration: const BoxDecoration(
                                      color: Color.fromRGBO(243, 249, 248, 1),
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(24),
                                        bottomRight: Radius.circular(24),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(),
                                ),
                              ],
                            ),
                            // image
                            Image.asset(productData["image"]),
                          ],
                        ),
                      ),
                      // description
                      Container(
                        width: double.maxFinite - 30,
                        margin: const EdgeInsets.symmetric(horizontal: 15),
                        child: Text(
                          productData["description"],
                          style: const TextStyle(
                            color: Color.fromRGBO(131, 195, 187, 1),
                            fontWeight: FontWeight.w500,
                            fontSize: 12,
                          ),
                        ),
                      ),
                      spaceH(25.0),
                      Container(
                        width: double.maxFinite - 30,
                        margin: const EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // shiing info
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                infoText(
                                    productData["shipping_info"]
                                        ["productWeight"],
                                    12.0),
                                spaceW(8.0),
                                infoText("|", 18.0),
                                spaceW(8.0),
                                infoText(
                                    productData["shipping_info"]
                                        ["shippingTime"],
                                    12.0),
                              ],
                            ),

                            // product stars info
                            Row(
                              children: [
                                Row(
                                  mainAxisSize: MainAxisSize.min,
                                  children: List.generate(
                                    5,
                                    (i) => Icon(
                                      Icons.star,
                                      color: i < productData["stars"].floor()
                                          ? const Color.fromRGBO(
                                              18, 140, 126, 1)
                                          : const Color.fromRGBO(
                                              131, 195, 187, 1),
                                      size: 15,
                                    ),
                                  ),
                                ),
                                spaceW(16.0),
                                infoText("${productData["stars"]}", 12.0),
                              ],
                            )
                          ],
                        ),
                      ),
                      spaceH(15.0),
                      // product price & count
                      Container(
                        width: double.maxFinite - 30,
                        margin: const EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            // count increment & decrement
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                // minus button
                                countActionButton(Icons.remove, "decrement",
                                    () {
                                  if (productCount > 1) {
                                    setState(() {
                                      productCount--;
                                    });
                                  }
                                }),
                                // count
                                Container(
                                  width: 30,
                                  height: 30,
                                  alignment: Alignment.center,
                                  child: Text(
                                    "$productCount",
                                    style: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400,
                                    ),
                                  ),
                                ),
                                // plus button
                                countActionButton(Icons.add, "increment", () {
                                  setState(() {
                                    productCount++;
                                  });
                                }),
                              ],
                            ),
                            // price
                            Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  "${productData["price"]}",
                                  style: const TextStyle(
                                    color: Color.fromRGBO(18, 140, 126, 1),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                spaceW(7.0),
                                Text(
                                  "price".tr(),
                                  style: const TextStyle(
                                    color: Color.fromRGBO(18, 140, 126, 1),
                                    fontSize: 9,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      spaceH(25.0),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            IsAnimatedImageTime = !IsAnimatedImageTime;
                            x = -0.2;
                            y = 1;
                            imgw = 0;
                            imgh = 0;
                          });
                        },
                        child: Container(
                          width: double.maxFinite - 30,
                          margin: const EdgeInsets.symmetric(horizontal: 15),
                          height: 40,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            color: const Color.fromRGBO(18, 140, 126, 1),
                            borderRadius: BorderRadius.circular(8),
                          ),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text(
                                "addToBasket".tr(),
                                style: const TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 16,
                                ),
                              ),
                              spaceW(10.0),
                              const Icon(
                                Icons.add_shopping_cart,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            homeBoldText('home_forYou'.tr()),
                            Text(
                              'home_seeAll'.tr(),
                              style: TextStyle(
                                  color: AppColors.unActive,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      SizedBox(
                          height: 210,
                          child: FutureBuilder<List<Allproducts>>(
                        future: data,
                        builder: (context,
                            AsyncSnapshot<List<Allproducts>> snapshot) {
                          if (!snapshot.hasData) {
                            return const CircularProgressIndicator();
                          }
                          return ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: productList.length,
                              itemBuilder: (context, index) {
                                return ForYouCard(
                                  name: snapshot.data![index].name ??
                                      'Default name',
                                  description:
                                      snapshot.data![index].description ??
                                          'Default description',
                                  price: snapshot.data![index].originalPrice ??
                                      'Default price',
                                  image: snapshot
                                          .data![index].defaultPhoto?.imgPath ??
                                      'Default Photo',
                                  viewCount: snapshot.data![index].touchCount ??
                                      'Default count',
                                  rateCount: snapshot.data![index].ratingDetails
                                          ?.totalRatingValue ??
                                      'Default name',
                                  favorite: true,
                                );
                              });
                        })),
                      const SizedBox(
                        height: 17,
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'home_special'.tr(),
                              style: const TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.w900),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            FutureBuilder<List<Allproducts>>(
                                future: data,
                                builder: (context,
                                    AsyncSnapshot<List<Allproducts>> snapshot) {
                                  if (!snapshot.hasData) {
                                    return CircularProgressIndicator();
                                  } else {
                                    return ListView.builder(
                                        itemCount: productList.length,
                                        shrinkWrap: true,
                                        physics: const NeverScrollableScrollPhysics(),
                                        itemBuilder: (context, index) {
                                          return ProductItem(
                                              name:
                                                  snapshot.data![index].name ??
                                                      'Undefined',
                                              description: snapshot.data![index]
                                                      .description ??
                                                  'Undefined',
                                              price: snapshot.data![index]
                                                      .originalPrice ??
                                                  'Undefined',
                                              image: snapshot.data![index]
                                                      .defaultPhoto!.imgPath ??
                                                  '',
                                              viewCount: snapshot.data![index]
                                                      .touchCount ??
                                                  'Undefined',
                                              rateCount: snapshot
                                                      .data![index]
                                                      .ratingDetails
                                                      ?.totalRatingValue ??
                                                  'Undefined',
                                              favorite: true);
                                        });
                                  }
                                }),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 24.0),
                              child: Center(
                                child: SeeMoreButton(
                                  onTap: () {},
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: double.maxFinite,
                  height: double.maxFinite,
                  child: AnimatedContainer(
                    duration: const Duration(milliseconds: 500),
                    alignment: Alignment(x, y),
                    child: AnimatedContainer(
                      duration: const Duration(milliseconds: 500),
                      width: imgw,
                      height: imgh,
                      child: IsAnimatedImageTime
                          ? Image.asset(
                              productData["image"],
                            )
                          : Container(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget spaceH(h) {
    return SizedBox(
      height: h,
    );
  }

  Widget spaceW(w) {
    return SizedBox(
      width: w,
    );
  }

  Widget infoText(String text, fSize) {
    return Text(
      text,
      style: TextStyle(
        color: const Color.fromRGBO(18, 140, 126, 1),
        fontSize: fSize,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  Widget countActionButton(icon, actionName, onPress) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        width: 30,
        height: 30,
        decoration: BoxDecoration(
          color: const Color.fromRGBO(243, 249, 248, 1),
          borderRadius: BorderRadius.circular(4),
        ),
        child: Center(
          child: Icon(
            icon,
            color: productCount == 1 && actionName == "decrement"
                ? const Color.fromARGB(255, 216, 216, 216)
                : const Color.fromRGBO(18, 140, 126, 1),
            size: 14,
          ),
        ),
      ),
    );
  }
}
