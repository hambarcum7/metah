import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:vanfood/ui/components/space.dart';
import 'package:vanfood/ui/components/widgets.dart';

import '../../../api_config/consturl.dart';
import '../../components/colors.dart';

class ProductItem extends StatefulWidget {
  const ProductItem({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite,
  }) : super(key: key);
  final String name;
  final String description;
  final String price;
  final String image;
  final String viewCount;
  final String rateCount;
  final bool favorite;
  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends State<ProductItem> {
 late bool favorite;
 @override
  void initState() {
    favorite = widget.favorite;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      padding: const EdgeInsets.only(right: 5),
      margin: const EdgeInsets.symmetric(vertical: 5),
      height: Space.space93,
      decoration: BoxDecoration(
          color: AppColors.restorantBackColor,
          boxShadow: const [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2,
              spreadRadius: 0.1,
              offset: Offset(0, 1),
            ),
          ],
          borderRadius: BorderRadius.circular(10)),
      child: FittedBox(
        fit: BoxFit.fill,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                AppUrl.subCategoryImageUrl + widget.image,
                fit: BoxFit.fitHeight,
                width: Space.space93,
              ),
            ),
            const SizedBox(
              width: Space.space17,
            ),
            Container(
              width: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    widget.name,
                    style: TextStyle(
                      color: AppColors.mainColorDark,
                      fontSize: Space.space14,
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  const SizedBox(
                    height: Space.space12,
                  ),
                  Text(
                    widget.description,
                    style: TextStyle(
                      color: AppColors.unActive,
                      fontSize: 10,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    widget.rateCount +
                        ' (' +
                        widget.viewCount +
                        'home_seeCount'.tr() +
                        ')',
                    style: TextStyle(
                      color: AppColors.mainColorLight,
                      fontSize: Space.space12,
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              width: Space.space28,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                    onTap: () {
                      setState(() {
                        favorite = !favorite;
                      });
                    },
                    child: widget.favorite
                        ? Icon(
                            Icons.favorite,
                            color: AppColors.mainColorLight,
                          )
                        : Icon(
                            Icons.favorite_border_outlined,
                            color: AppColors.mainColorLight,
                          )),
                const SizedBox(
                  height: 51,
                ),
                Text(
                  '1500price'.tr(),
                  style: TextStyle(
                    fontSize: 12,
                    color: AppColors.mainColorLight,
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
