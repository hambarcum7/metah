import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/api_config/consturl.dart';
import '../../../models/all_sub_category_model.dart';
import '../../../provider/provider_all_sub_categorys.dart';
import '../../components/back_button.dart';
import '../../components/search_input.dart';

class Detail extends StatelessWidget {
  const Detail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => VeiwModelAllSubCategorys(),
      child: DetailPage(
        restaurantName: 'Elkani',
      ),
    );
  }
}

class DetailPage extends StatefulWidget {
  DetailPage({Key? key, required this.restaurantName})
      : super(key: key);
  String restaurantName;
  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  List<AllSubCategory> filteredCategories = [];

  TextEditingController searchTextController = TextEditingController();

  late Future<List<AllSubCategory>> data;

  Future<List<AllSubCategory>> getAllSubCategoriesData() async {
    List<dynamic> response = await VeiwModelAllSubCategorys().getSpecialInfo();
    return response.map((e) => AllSubCategory.fromJson(e)).toList();
  }

  @override
  void initState() {
    super.initState();
    data = getAllSubCategoriesData();
    data.then((value) => filteredCategories = value);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          child: Container(
            width: double.maxFinite - 30,
            height: double.maxFinite,
            margin: const EdgeInsets.symmetric(horizontal: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceH(25.0),
                // back button
                BackToButton(
                  backPageName: "home_restaurants".tr(),
                  press: () {
                    Navigator.pop(context);
                  },
                ),
                spaceH(18.0),
                // page title
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.restaurantName,
                      style: const TextStyle(
                        color: Color.fromRGBO(18, 140, 126, 1),
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ],
                ),
                spaceH(15.0),
                // search
                SearchInput(
                  controller: searchTextController,
                  press: () async {
                    List<AllSubCategory> items = await data;
                    setState(() {
                      filteredCategories = [];
                      for (AllSubCategory item in items) {
                        if (item.name.toLowerCase().contains(searchTextController.text.toLowerCase())) {
                          filteredCategories.add(item);
                        }
                      }
                    });
                  },
                ),
                spaceH(15.0),
                Expanded(
                  child: FutureBuilder<List<AllSubCategory>>(
                      future: data,
                      builder: (context,
                          AsyncSnapshot<List<AllSubCategory>> snapshot) {
                        if (!snapshot.hasData) {
                          return const Center(
                              child: CircularProgressIndicator());
                        }
                        return ListView.builder(
                            itemCount: filteredCategories.length,
                            itemBuilder: (BuildContext context, int i) {
                              return Container(
                                width: size.width - 30,
                                height: (size.width - 30) / 2.25,
                                child: Stack(
                                  children: [
                                    Container(
                                      width: size.width - 30,
                                      height: (size.width - 30) / 2.25,
                                      margin: const EdgeInsets.only(
                                        bottom: 8,
                                      ),
                                      decoration: BoxDecoration(
                                        color: Colors.red,
                                        borderRadius: BorderRadius.circular(8),
                                        image: DecorationImage(
                                          image: NetworkImage(
                                            AppUrl.subCategoryImageUrl +
                                                filteredCategories[i]
                                                    .defaultPhoto!
                                                    .imgPath,
                                          ),
                                          fit: BoxFit.fill,
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: size.width - 30,
                                      height: (size.width - 30) / 2.25,
                                      margin: const EdgeInsets.only(
                                        bottom: 8,
                                      ),
                                      decoration: BoxDecoration(
                                        color:
                                            const Color.fromRGBO(0, 0, 0, 0.4),
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          const Spacer(),
                                          // restaurant name
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                width: size.width - 80,
                                                alignment: Alignment.center,
                                                child: Text(
                                                  filteredCategories[i].name,
                                                  style: const TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ],
                                          ),
                                          spaceH(15.0)
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            });
                      }),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget spaceW(size) {
    return SizedBox(
      width: size,
    );
  }

  Widget spaceH(size) {
    return SizedBox(
      height: size,
    );
  }
}
