import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../components/colors.dart';
import '../basket_page/basket_page.dart';
import '../choose_product/choose_product.dart';
import '../profile/profile.dart';
import '../special/special.dart';


class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  static final List<Widget> _widgetOptions = <Widget>[
    Special(),
    const BasketPage(),
    const ChooseProduct(),
    const ProfilePage(),
  ];
  int currentIndex = 0;
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
  
  Future<void> getCurrentToken() async{
    // String token = await FirebaseAuth.instance.currentUser!.getIdToken();
    // print("start Token :  " + token + "   end token");
  }

@override
  void initState() {
    super.initState();
    // getCurrentToken();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          width: MediaQuery.of(context).size.width,
          height: 61,
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            color: AppColors.light,
          ),
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              GestureDetector(
                  onTap: () {
                    updateIndex(0);
                    _onItemTapped(0);
                  },
                  child: Column(
                    children: [
                      Icon(
                        Icons.home,
                        color: currentIndex == 0
                            ? AppColors.mainColorDark
                            : AppColors.mainColorLight,
                      ),
                      Text(
                        'navbar_main'.tr(),
                        style: TextStyle(
                          color: currentIndex == 0
                              ? AppColors.mainColorDark
                              : AppColors.mainColorLight,
                        ),
                      ),
                    ],
                  )),
              GestureDetector(
                  onTap: () {
                    updateIndex(1);
                    _onItemTapped(1);
                  },
                  child: Column(
                    children: [
                      Icon(
                        Icons.shopping_cart_rounded,
                        color: currentIndex == 1
                            ? AppColors.mainColorDark
                            : AppColors.mainColorLight,
                      ),
                      Text(
                        'navbar_busket'.tr(),
                        style: TextStyle(
                          color: currentIndex == 1
                              ? AppColors.mainColorDark
                              : AppColors.mainColorLight,
                        ),
                      ),
                    ],
                  )),
              GestureDetector(
                  onTap: () {
                    updateIndex(2);
                    _onItemTapped(2);
                  },
                  child: Column(
                    children: [
                      Column(
                        children: [
                          Icon(
                            Icons.favorite_rounded,
                            color: currentIndex == 2
                                ? AppColors.mainColorDark
                                : AppColors.mainColorLight,
                          ),
                          Text(
                            'navbar_favorite'.tr(),
                            style: TextStyle(
                              color: currentIndex == 2
                                  ? AppColors.mainColorDark
                                  : AppColors.mainColorLight,
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
              GestureDetector(
                onTap: () {
                  updateIndex(3);
                  _onItemTapped(3);
                },
                child: Column(
                  children: [
                    Icon(
                      Icons.gpp_good,
                      color: currentIndex == 3
                          ? AppColors.mainColorDark
                          : AppColors.mainColorLight,
                    ),
                    Text(
                      'navbar_profile'.tr(),
                      style: TextStyle(
                        color: currentIndex == 3
                            ? AppColors.mainColorDark
                            : AppColors.mainColorLight,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void updateIndex(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
