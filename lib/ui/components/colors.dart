import 'package:flutter/material.dart';

class AppColors {
  static Color mainColorLight = const Color(0xff128C7E);
  static Color mainColorDark = const Color(0xff044039);
  static Color vanFood = const Color(0xff32D9C5);
  static Color unActive = const Color(0xff83C3BB);
  static Color light = const Color(0xffFFFFFF);
  static Color dark = const Color(0xff000000);
  static Color indikatorUnActive = const Color(0xff7EC5BF);
  static Color activeRed = const Color(0xffE30000);
  static Color colorsSocialNetworks = const Color(0xffD9EDEA);
  static Color searchColor = const Color(0xff4FADA3);
  static Color restorantBackColor = const Color(0xffF3F9F8);
  static Color circul = const Color(0xffBDF3ED);
}