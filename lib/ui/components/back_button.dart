import 'package:flutter/material.dart';

// ignore: must_be_immutable
class BackToButton extends StatelessWidget {
  BackToButton({
    Key? key,
    required this.backPageName,
    required this.press,
    this.color = const Color.fromRGBO(4, 64, 57, 1),
  }) : super(key: key);
  String backPageName;
  VoidCallback press;
  Color color;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Icon(
            Icons.chevron_left,
            color: color,
          ),
          const SizedBox(
            width: 8,
          ),
          Text(
            backPageName,
            style: TextStyle(
              color: color,
              fontSize: 16,
              fontWeight: FontWeight.w700,
            ),
          )
        ],
      ),
    );
  }
}
