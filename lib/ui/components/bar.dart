import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'colors.dart';
class MyBar extends StatefulWidget {
  
  const MyBar({
    Key? key,
  }) : super(key: key);

  @override
  _MyBar createState() => _MyBar();
}

class _MyBar extends State<MyBar> {
  bool myvalue =false;
  int currentIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 60,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: AppColors.light,
      ),
      padding: const EdgeInsets.all(8.0),
      child: Row(
       mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
              onTap: () {
                updateIndex(0);
              },
              child: Column(
                children: [
                  Icon(
                    Icons.home,
                    color: currentIndex == 0
                        ? AppColors.mainColorDark
                        : AppColors.mainColorLight,
                  ),
                  Text(
                    'navbar_main'.tr(),
                    style: TextStyle(
                      color: currentIndex == 0
                          ? AppColors.mainColorDark
                          : AppColors.mainColorLight,
                    ),
                  ),
                ],
              )),
          GestureDetector(
              onTap: () {
                updateIndex(1);
              },
              child: Column(
                children: [
                  Stack(
                    children: [
                       Icon(
                    Icons.shopping_cart_rounded,
                    color: currentIndex == 1
                        ? AppColors.mainColorDark
                        : AppColors.mainColorLight,
                  ),
                  Positioned(
                    top: 0,
                    right: 0,
                    child: Container(
                      decoration: BoxDecoration(
                        color: myvalue?AppColors.activeRed:Colors.transparent,
                      ),
                    ),
                  )
                    ],
                  ),
                 
                  Text(
                    'navbar_busket'.tr(),
                    style: TextStyle(
                      color: currentIndex == 1
                          ? AppColors.mainColorDark
                          : AppColors.mainColorLight,
                    ),
                  ),
                ],
              )),
          GestureDetector(
              onTap: () {
                updateIndex(2);
              },
              child: Column(
                children: [
                  Column(
                    children: [
                      Icon(
                        Icons.favorite_rounded,
                        color: currentIndex == 2
                            ? AppColors.mainColorDark
                            : AppColors.mainColorLight,
                      ),
                      Text(
                        'navbar_favorite'.tr(),
                        style: TextStyle(
                          color: currentIndex == 2
                              ? AppColors.mainColorDark
                              : AppColors.mainColorLight,
                        ),
                      ),
                    ],
                  ),
                ],
              )),
          GestureDetector(
              onTap: () {
                updateIndex(3);
              },
              child: Column(
                children: [
                  Icon(
                    Icons.gpp_good,
                    color: currentIndex == 3
                        ? AppColors.mainColorDark
                        : AppColors.mainColorLight,
                  ),
                  Text(
                    'navbar_profile'.tr(),
                    style: TextStyle(
                      color: currentIndex == 3
                          ? AppColors.mainColorDark
                          : AppColors.mainColorLight,
                    ),
                  ),
                ],
              )),
        ],
      ),
    );
  }

  void updateIndex(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}
