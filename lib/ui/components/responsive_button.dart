import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ResponsiveButton extends StatelessWidget {
  ResponsiveButton({Key? key,required this.isFilled,required this.callback}) : super(key: key);
  bool isFilled;
  VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Container(
          height: 50,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: isFilled ?  const Color.fromRGBO(18, 140, 126, 1) : const Color.fromRGBO(18, 140, 126, 0),
            borderRadius: BorderRadius.circular(12),
          ),
          child: Text(
            isFilled ?
            "Հաջորդ": "Թողնել",
            style: TextStyle(
              color: isFilled ? Colors.white : const Color.fromRGBO(79, 173, 163, 1),
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
      ),
    );
  }
}