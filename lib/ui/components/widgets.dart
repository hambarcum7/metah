import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/provider/provider_all_categorys.dart';

import '../../api_config/consturl.dart';
import '../screen/login_page/login_page.dart';
import '../screen/product_detail/product_detail.dart';
import '../screen/resturant/restaurants.dart';
import 'colors.dart';

Widget profileImage() {
  return SizedBox(
    child: Image.asset('assets/img/avatar.png'),
    width: 100,
    height: 100,
  );
}

Widget profileText(
  String text,
  Color textColor,
) {
  return Text(
    text,
    style: TextStyle(
        color: textColor,
        fontWeight: FontWeight.w700,
        fontSize: 18,
        fontFamily: 'Mardoto'),
  );
}

Widget profileItem(
  String text,
  callback
) {
  return GestureDetector(
    onTap: callback,
    child: Container(
      padding: const EdgeInsets.only(bottom: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: Text(
              text,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w400,
                  color: AppColors.mainColorDark,
                  fontFamily: 'Mardoto'),
            ),
          ),
          const Icon(CupertinoIcons.right_chevron),
        ],
      ),
    ),
  );
}

Widget homeBoldText(String text) {
  return Text(
    text,
    style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.w900,
        color: AppColors.mainColorDark),
  );
}

Widget homeButton() {
  return Container(
    width: 150,
    padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
    child: Row(
      children: [
        Text(
          'seeMore'.tr(),
          style: const TextStyle(
              color: Colors.white, fontWeight: FontWeight.w700, fontSize: 14),
        ),
        const Icon(
          CupertinoIcons.right_chevron,
          color: Colors.white,
        ),
      ],
    ),
    decoration: BoxDecoration(
        color: AppColors.mainColorLight,
        borderRadius: BorderRadius.circular(8)),
  );
}

class Salades extends StatefulWidget {
  const Salades({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite, 
  }) : super(key: key);
  final String name;
  final String description;
  final String price;
  final String image;
  final String viewCount;
  final String rateCount;
  final bool favorite;

  @override
  _SaladesState createState() => _SaladesState();
}

class _SaladesState extends State<Salades> {
  bool favorite = false;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 16),
            width: 206,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const ProductDetail()));
                  },
                  child: SizedBox(
                    child: Column(
                      children: [
                        SizedBox(
                          height: 120,
                          child: ClipRRect(
                            child: Image.network(AppUrl.subCategoryImageUrl + widget.image),
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8)),
                          ),
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                                child: Text(
                              widget.name,
                              style: const TextStyle(
                                  fontWeight: FontWeight.w500, fontSize: 12),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            )),
                            const SizedBox(
                              width: 5,
                            ),
                            Container(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 3, horizontal: 12),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: AppColors.mainColorLight,
                                ),
                                child: Text(widget.price,
                                    style: const TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w700,
                                    ))),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  'Մադեռա սրճարան / Ուտեստներ',
                  style: TextStyle(
                      color: AppColors.mainColorLight,
                      fontSize: 10,
                      fontWeight: FontWeight.w500),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        stars(),
                        Text(
                          '4.8 (112 դիտում) ',
                          style: TextStyle(
                              color: AppColors.mainColorLight,
                              fontSize: 10,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    smallPhotos()
                  ],
                ),
              ],
            ),
          ),
        ),
        Positioned(
            right: 30,
            bottom: 222,
            child: GestureDetector(
              onTap: () {
                setState(() {
                  favorite = !favorite;
                });
              },
              child: Container(
                padding: const EdgeInsets.all(6),
                child: Icon(
                  favorite ? Icons.favorite : Icons.favorite_border,
                  color: Colors.white,
                  size: 22,
                ),
                decoration: BoxDecoration(
                  color: AppColors.mainColorLight,
                  borderRadius: BorderRadius.circular(100),
                ),
              ),
            ))
      ],
    );
  }
}

Widget stars() {
  return Row(
    children: List.generate(
        5,
        (index) => Icon(
              Icons.star_rate_sharp,
              color: AppColors.mainColorLight,
              size: 20,
            )),
  );
}

Widget smallPhotos() {
  return Container(
    margin: const EdgeInsets.only(right: 5),
    width: 70,
    child: Stack(
      children: [
        Container(
          width: 22,
          height: 22,
          child: Image.asset('assets/img/mard1.png'),
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(100)),
        ),
        Positioned(
          left: 15,
          child: Container(
            width: 22,
            height: 22,
            child: Image.asset('assets/img/mard2.png'),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
            ),
          ),
        ),
        Positioned(
          left: 30,
          child: Container(
            width: 22,
            height: 22,
            child: Image.asset('assets/img/mard3.png'),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
            ),
          ),
        ),
      ],
    ),
  );
}

widgetrest(context, String text, String nkar) {
  return GestureDetector(
    onTap: () {
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => RestaurantsScreenState() ));
    },
    child: Container(
      decoration: BoxDecoration(
          color: AppColors.restorantBackColor,
          borderRadius: BorderRadius.circular(8)),
      margin: const EdgeInsets.only(
        top: 15,
        right: 5,
      ),
      width: 200,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: SizedBox(
              child: Image.asset(
                nkar,
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              alignment: Alignment.center,
              child: Text(
                text,
                style: TextStyle(
                  color: AppColors.mainColorLight,
                ),
              ),
            ),
          ),
        ],
      ),
    ),
  );
}

widgetProduct(context,callback) {
  return Container(
    margin: const EdgeInsets.only(
      top: 15,
      right: 5,
    ),
    width: 320,
    decoration: BoxDecoration(
      color: AppColors.mainColorDark,
      borderRadius: BorderRadius.circular(10),
    ),
    child: Row(
      children: [
        Expanded(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  child: Text(
                    'Իքիբիր, կծու սուսով',
                    style: TextStyle(
                      fontSize: 12,
                      color: AppColors.light,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(
                    top: 5,
                  ),
                  child: Text(
                    'Գնիր հիմա և Էլկանի ռեստորանի կողմից ստացիր նվեր․․․',
                    softWrap: true,
                    style: TextStyle(
                      color: AppColors.light,
                      fontSize: 10,
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: callback,
                  //  () {
                  //   Navigator.push(
                  //       context,
                  //       MaterialPageRoute(
                  //           builder: (context) => LoginPage()));
                  // },
                  child: Container(
                    width: 110,
                    padding: const EdgeInsets.only(
                      top: 6,
                      bottom: 6,
                    ),
                    margin: const EdgeInsets.only(
                      top: 5,
                    ),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: AppColors.mainColorLight,
                    ),
                    child: Text(
                      'Գնել հիմա',
                      style: TextStyle(
                        fontSize: 13,
                        color: AppColors.light,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Expanded(
          child: SizedBox(
            child: Image.asset(
              'assets/product_image.png',
            ),
          ),
        ),
      ],
    ),
  );
}

containerText(
    String text, double size, Color textColor, double margTop, FontWeight fw) {
  return Container(
    margin: EdgeInsets.only(
      top: margTop,
    ),
    child: Text(
      text,
      style: TextStyle(
        color: textColor,
        fontSize: size,
        fontWeight: fw,
      ),
    ),
  );
}

widgetBuyProduct(context, String text, Color textColor, double textSize,
    Color contColor, FontWeight fontWeight, double contTop) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
      color: contColor,
    ),
    margin: EdgeInsets.only(
      top: contTop,
      left: 16.0,
      right: 16.0,
    ),
    padding: const EdgeInsets.symmetric(
    vertical: 10,
    ),
    alignment: Alignment.center,
   // width: MediaQuery.of(context).size.width - 32,
    child: Text(
      text,
      style: TextStyle(
        fontSize: textSize,
        color: textColor,
        fontWeight: fontWeight,
      ),
    ),
  );
}

widgetBuyProductChoose(context, String text, Color textColor, double textSize,
    Color contColor, FontWeight fontWeight, double contTop, String image) {
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(8),
      color: contColor,
    ),
    margin: EdgeInsets.only(
      top: contTop,
    ),
    padding: const EdgeInsets.only(
      top: 10,
      bottom: 10,
    ),
    alignment: Alignment.center,
    width: MediaQuery.of(context).size.width - 32,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          text,
          style: TextStyle(
            fontSize: textSize,
            color: textColor,
            fontWeight: fontWeight,
          ),
        ),
        Container(
            margin: const EdgeInsets.only(
              left: 6,
            ),
            child: Image.asset(image)),
      ],
    ),
  );
}

widgetPrice(String textLeft, String textRight, double textSize, Color textColor,
    FontWeight textWeight) {
  return Container(
    margin: const EdgeInsets.only(
      top: 15,
    ),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        SizedBox(
          child: Text(
            textLeft,
            style: TextStyle(
              color: textColor,
              fontSize: textSize,
              fontWeight: textWeight,
            ),
          ),
        ),
        SizedBox(
          child: Text(
            textRight,
            style: TextStyle(
              color: AppColors.unActive,
              fontSize: textSize,
              fontWeight: textWeight,
            ),
          ),
        ),
      ],
    ),
  );
}

class FavoriteProduct extends StatefulWidget {
  const FavoriteProduct({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite,
  }) : super(key: key);
  final String name;
  final String description;
  final String price;
  final String image;
  final String viewCount;
  final String rateCount;
  final bool favorite;

  @override
  _FavoriteProductState createState() => _FavoriteProductState();
}

class _FavoriteProductState extends State<FavoriteProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85,
      padding: const EdgeInsets.all(6),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: AppColors.light,
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(255, 211, 241, 236),
            spreadRadius: 0,
            blurRadius: 2,
            offset: Offset(-3, 3),
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: SizedBox(
              child: Image.asset(
                widget.image,
              ),
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(
              padding: const EdgeInsets.only(
                left: 6,
                right: 6,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  sizedBoxWidget(widget.name, 14, AppColors.mainColorDark,
                      FontWeight.bold, 2),
                  sizedBoxWidget(widget.description, 10, AppColors.unActive,
                      FontWeight.normal, 1),
                  sizedBoxWidget(widget.price, 14, AppColors.mainColorLight,
                      FontWeight.bold, 1),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: Center(
                child: Image.asset(
              'assets/x.png',
            )),
          ),
        ],
      ),
    );
  }
}

blockProductBasketChoose() {}

class BasketProduct extends StatefulWidget {
  const BasketProduct({
    Key? key,
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite,
    required this.selectedCount,
    required this.onTap,
  }) : super(key: key);
  final String name;
  final String description;
  final String price;
  final String image;
  final String viewCount;
  final String rateCount;
  final bool favorite;
  final int selectedCount;
  final VoidCallback onTap;
  @override
  _BasketProductState createState() => _BasketProductState();
}

class _BasketProductState extends State<BasketProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 85,
      padding: const EdgeInsets.all(6),
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: AppColors.light,
        boxShadow: const [
          BoxShadow(
            color: Color.fromARGB(255, 211, 241, 236),
            spreadRadius: 0,
            blurRadius: 2,
            offset: Offset(-3, 3),
          ),
        ],
      ),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              child:  Text(
                widget.selectedCount.toString() + ' x',
                maxLines: 1,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: SizedBox(
              child: Image.asset(
                widget.image,
              ),
            ),
          ),
          Expanded(
            flex: 6,
            child: Container(
              padding: const EdgeInsets.only(
                left: 6,
                right: 6,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  sizedBoxWidget(widget.name, 14,
                      AppColors.mainColorDark, FontWeight.bold, 2),
                  sizedBoxWidget(widget.description, 10, AppColors.unActive,
                      FontWeight.normal, 1),
                  sizedBoxWidget(widget.price, 14, AppColors.mainColorLight,
                      FontWeight.bold, 1),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: GestureDetector(
              onTap: widget.onTap,
              child: Center(
                  child: Image.asset(
                'assets/x.png',
              )),
            ),
          ),
        ],
      ),
    );
  }
}

sizedBoxWidget(String text, double sizeText, Color colorText,
    FontWeight theWeight, int maxLine) {
  return SizedBox(
    child: Text(
      text,
      maxLines: maxLine,
      style: TextStyle(
        fontSize: sizeText,
        color: colorText,
        fontWeight: theWeight,
      ),
    ),
  );
}

class Product {
  String name;
  String description;
  String price;
  String image;
  String viewCount;
  String rateCount;
  bool favorite;
  int selectedCount;
  Product({
    required this.name,
    required this.description,
    required this.price,
    required this.image,
    required this.viewCount,
    required this.rateCount,
    required this.favorite,
    required this.selectedCount,
  });
}

Product brokoli = Product(
  name: 'food_brocoli'.tr(),
  description: 'rest_madera'.tr(),
  price: '1.500',
  image: 'assets/img/fooditem.png',
  viewCount: '10',
  rateCount: '4.7',
  favorite: false,
  selectedCount: 2,
);
Product brokol2 = Product(
  name: 'food_brocolisdfsdf'.tr(),
  description: 'rest_madera'.tr(),
  price: '1.500',
  image: 'assets/img/fooditem.png',
  viewCount: '120',
  rateCount: '4.7',
  favorite: true,
  selectedCount: 2,
);

Product brokol3 = Product(
  name: 'food_broc'.tr(),
  description: 'rest_madera'.tr(),
  price: '1.500',
  image: 'assets/products.png',
  viewCount: '8',
  rateCount: '4.7',
  favorite: false,
  selectedCount: 2,
);

List<Product> productList = [
  brokoli,
  brokol2,
  brokol3,
  brokol2,
  brokoli,
  brokol2,
  brokol3,
  brokol2,
];

class SeeMoreButton extends StatelessWidget {
  const SeeMoreButton({
    Key? key,
    required this.onTap,
  }) : super(key: key);
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        decoration: BoxDecoration(
          color: AppColors.mainColorLight,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('seeMore'.tr(),
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 14,
                    fontWeight: FontWeight.w400)),
            const Icon(
              CupertinoIcons.right_chevron,
              color: Colors.white,
            ),
          ],
        ),
      ),
    );
  }
}
