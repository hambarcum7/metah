import 'package:flutter/material.dart';

class SearchInput extends StatelessWidget {
  SearchInput({Key? key, required this.controller , required this.press }) : super(key: key);
  TextEditingController controller;
  VoidCallback press;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        // textfield
        Expanded(
          flex: 5,
          child: Container(
            height: 40,
            child: TextField(
              controller: controller,
              decoration: const InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color.fromRGBO(243, 249, 248, 1),
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Color.fromRGBO(243, 249, 248, 1),
                  ),
                ),
                filled: true,
                fillColor: Color.fromRGBO(243, 249, 248, 1),
                hintText: "Արագ որոնում․․․",
                hintStyle: TextStyle(
                  color: Color.fromRGBO(79, 173, 163, 1),
                ),
              ),
            ),
          ),
        ),
        // searchButton
        Expanded(
          child: GestureDetector(
            onTap: press,
            child: Container(
              alignment: Alignment.center,
              height: 40,
              decoration: const BoxDecoration(
                color: Color.fromRGBO(18, 140, 126, 1),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(8),
                  bottomRight: Radius.circular(8),
                ),
              ),
              child: const Icon(
                Icons.search,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
