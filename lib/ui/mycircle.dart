import 'dart:math' as math;
import 'package:flutter/material.dart';
import 'package:vanfood/ui/components/colors.dart';
import 'package:vanfood/ui/mypage.dart';
import 'package:vanfood/ui/screen/your_payment_completed/your_payment_completed.dart';

class MyAnime extends StatefulWidget {
  const MyAnime({Key? key}) : super(key: key);

  @override
  State<MyAnime> createState() => _MyAnime();
}

class _MyAnime extends State<MyAnime> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  late Animation<double> _animation;
  final maxProgres = 100.0;
  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    );
    _animation =
        Tween<double>(begin: 0, end: maxProgres).animate(_animationController)
          ..addListener(
            () {
              setState(() {});
            },
          );
    _animationController.forward().whenComplete(
      () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const PaymentCompleted(),
          ),
        );
      },
    );
  }

  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                width: double.maxFinite,
                height: double.maxFinite,
                child: Image.asset(
                  'assets/fon.png',
                  fit: BoxFit.cover,
                ),
              ),
              Center(
                child: CustomPaint(
                  foregroundPainter: CircleProgress(_animation.value),
                  child: Container(
                    width: 150,
                    height: 150,
                    child: Center(
                      child: Icon(
                        Icons.check,
                        size: 150,
                        color: _animation.value == maxProgres
                            ? AppColors.mainColorLight
                            : Colors.transparent,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
