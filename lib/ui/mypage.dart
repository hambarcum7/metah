import 'dart:math';
import 'package:flutter/material.dart';
import 'package:vanfood/ui/components/colors.dart';

class CircleProgress extends CustomPainter {
  double strokeCircle = 13;
  double currentProgress;
  CircleProgress(this.currentProgress);
  @override
  void paint(Canvas canvas, Size size) {
    Paint circle = Paint()
      ..strokeWidth = strokeCircle
      ..color = AppColors.circul
      ..style = PaintingStyle.stroke;
    Offset center = Offset(size.width / 2, size.height / 2);
    double radius = 90;
    canvas.drawCircle(center, radius, circle);
    Paint animationArc = Paint()
      ..strokeWidth = strokeCircle
      ..color = AppColors.mainColorLight
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
    double angle = 2 * pi * (currentProgress / 100);
    canvas.drawArc(Rect.fromCircle(center: center, radius: radius), pi / 2,
        angle, false, animationArc);
  }

  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
