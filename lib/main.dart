import 'dart:async';
import 'dart:developer' as developer;
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:vanfood/provider/login_view_model.dart';
import 'package:vanfood/ui/screen/home_page/home_page.dart';
import 'package:vanfood/ui/screen/login_page/login_page.dart';
import 'package:vanfood/ui/screen/moto_anime/motoanime.dart';
import 'package:vanfood/ui/screen/product_detail/product_detail.dart';
import 'package:vanfood/ui/screen/profile/profile.dart';
import 'package:vanfood/ui/screen/register/register_page.dart';
import 'package:vanfood/ui/screen/resturant/restaurants.dart';
import 'package:vanfood/ui/screen/salades_page/salades_page.dart';
import 'package:vanfood/ui/screen/your_payment_completed/esiminch.dart';
import 'ui/components/colors.dart';
import 'ui/screen/on_bording/onboarding_page_template.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: AppColors.mainColorLight, // status bar color
  ));
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: true,
      theme: ThemeData(
        fontFamily: 'Mardoto',
      ),
      home: EasyLocalization(
          startLocale: const Locale('hy'),
          child: MyApp(),
          supportedLocales: const [Locale('hy'), Locale('ru'), Locale('en')],
          path: 'assets/languages'),
    ),
  );
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

ConnectivityResult connectionStatus = ConnectivityResult.none;
final Connectivity connectivity = Connectivity();
ConnectivityResult? connectivityResult;
late StreamSubscription<ConnectivityResult> connectivitySubscription;

class _MyAppState extends State<MyApp> {
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    try {
      result = await connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      developer.log('Couldn\'t check connectivity status', error: e);
      return;
    }
    if (!mounted) {
      return Future.value(null);
    }

    return updateConnectionStatus(result);
  }

  @override
  void initState() {
    initConnectivity();
    // connectivitySubscription =
    //     connectivity.onConnectivityChanged.listen(updateConnectionStatus);
    super.initState();
  }

  Future<void> updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      connectionStatus = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    // return MyApppp();
    // print(connectionStatus);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      supportedLocales: context.supportedLocales,
      localizationsDelegates: context.localizationDelegates,
      locale: context.locale,
      home: const HomePage(),
    );
  }
}
