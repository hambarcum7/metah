import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import '../models/all_cotegory_model.dart';
import '../models/all_sub_category_model.dart';
import 'consturl.dart';
import 'consturl_keys.dart';

class RequestGet {
  static Future<dynamic> getData(String url, Map<String, dynamic> map) async {
     final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      final responseDecode = jsonDecode(response.body) as List;
      return responseDecode;
    } else {
      throw Exception('Erorr Get');
    }
  }

  static Future<dynamic> postData(String url, dynamic body) async {
    final response = await http.post(
      Uri.parse(url),
      body: const JsonEncoder().convert(body),
      headers: { 'Content-Type': 'application/json; charset=UTF-8',},
    );
    if (response.statusCode >= 200 && response.statusCode < 300) {

      return jsonDecode(response.body);
    } else {
      throw Exception('Erorr Post');
    }
  }
}
//http://develop.biomart.su/index.php/rest/categories/search/api_key/teampsisthebest/limit/15/offset/0
//http://develop.biomart.su/index.php/rest/subcategories/get/api_key/teampsisthebest/limit/15/offset/0/cat_id/catca78aaf2ffbac9a4fc04c9741b45e653


