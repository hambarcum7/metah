class AppUrl {
  AppUrl();

  static const String app_url = 'http://develop.biomart.su/index.php';
  static const String app_all_categories =
      app_url + '/rest/categories/search/api_key/' + api_key;
  static const String app_all_subcategories =
      app_url + '/rest/subcategories/get/api_key/' + api_key;
  static const String app_all_products =
      app_url + '/rest/products/search/api_key/' + api_key;
  static const String app_products_or_single_products_detail =
      app_url + '/rest/products/get/api_key/' + api_key;
  static const String app_relaited_products =
      app_url + '/related_product_trending/api_key/' + api_key;
  static const String app_product_colection =
      app_url + '/rest/collections/get/api_key/' + api_key;
  static const String subCategoryImageUrl = "http://develop.biomart.su/uploads/";
  static const String limit = 'limit/';
  static const String offset = '/offset/';
  static const String cat_id = '/cat_id/';
  static const String id = 'id/';
  static const String login_user_id = '/login_user_id/';
  static const String api_key = 'teampsisthebest/';
}
