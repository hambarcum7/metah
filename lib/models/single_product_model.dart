class SingleProduct {
  String? id;
  String? catId;
  String? subCatId;
  String? productUnit;
  String? name;
  String? description;
  String? originalPrice;
  String? unitPrice;
  String? minimumOrder;
  String? searchTag;
  String? highlightInformation;
  String? isDiscount;
  String? isFeatured;
  String? isAvailable;
  String? code;
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;
  String? overallRating;
  String? touchCount;
  String? favouriteCount;
  String? likeCount;
  String? featuredDate;
  String? ordering;
  String? ingredient;
  String? nutrient;
  String? maximumOrder;
  String? dynamicLink;
  String? addedDateStr;
  String? transStatus;
  DefaultPhoto? defaultPhoto;
  Category? category;
  SubCategory? subCategory;
  List<Addon>? addon;
  List<Specs>? specs;
  String? isLiked;
  String? isFavourited;
  String? imageCount;
  String? commentHeaderCount;
  String? currencySymbol;
  String? currencyShortForm;
  String? discountAmount;
  String? discountPercent;
  String? discountValue;
  List<CustomizedHeader>? customizedHeader;
  RatingDetails? ratingDetails;

  SingleProduct(
      {this.id,
      this.catId,
      this.subCatId,
      this.productUnit,
      this.name,
      this.description,
      this.originalPrice,
      this.unitPrice,
      this.minimumOrder,
      this.searchTag,
      this.highlightInformation,
      this.isDiscount,
      this.isFeatured,
      this.isAvailable,
      this.code,
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag,
      this.overallRating,
      this.touchCount,
      this.favouriteCount,
      this.likeCount,
      this.featuredDate,
      this.ordering,
      this.ingredient,
      this.nutrient,
      this.maximumOrder,
      this.dynamicLink,
      this.addedDateStr,
      this.transStatus,
      this.defaultPhoto,
      this.category,
      this.subCategory,
      this.addon,
      this.specs,
      this.isLiked,
      this.isFavourited,
      this.imageCount,
      this.commentHeaderCount,
      this.currencySymbol,
      this.currencyShortForm,
      this.discountAmount,
      this.discountPercent,
      this.discountValue,
      this.customizedHeader,
      this.ratingDetails});

  SingleProduct.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    catId = json['cat_id'];
    subCatId = json['sub_cat_id'];
    productUnit = json['product_unit'];
    name = json['name'];
    description = json['description'];
    originalPrice = json['original_price'];
    unitPrice = json['unit_price'];
    minimumOrder = json['minimum_order'];
    searchTag = json['search_tag'];
    highlightInformation = json['highlight_information'];
    isDiscount = json['is_discount'];
    isFeatured = json['is_featured'];
    isAvailable = json['is_available'];
    code = json['code'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    overallRating = json['overall_rating'];
    touchCount = json['touch_count'];
    favouriteCount = json['favourite_count'];
    likeCount = json['like_count'];
    featuredDate = json['featured_date'];
    ordering = json['ordering'];
    ingredient = json['ingredient'];
    nutrient = json['nutrient'];
    maximumOrder = json['maximum_order'];
    dynamicLink = json['dynamic_link'];
    addedDateStr = json['added_date_str'];
    transStatus = json['trans_status'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    category =
        json['category'] != null ? Category.fromJson(json['category']) : null;
    subCategory = json['sub_category'] != null
        ? SubCategory.fromJson(json['sub_category'])
        : null;
    if (json['addon'] != null) {
      addon = <Addon>[];
      json['addon'].forEach((v) {
        addon!.add(Addon.fromJson(v));
      });
    }
    if (json['specs'] != null) {
      specs = <Specs>[];
      json['specs'].forEach((v) {
        specs!.add(Specs.fromJson(v));
      });
    }
    isLiked = json['is_liked'];
    isFavourited = json['is_favourited'];
    imageCount = json['image_count'];
    commentHeaderCount = json['comment_header_count'];
    currencySymbol = json['currency_symbol'];
    currencyShortForm = json['currency_short_form'];
    discountAmount = json['discount_amount'];
    discountPercent = json['discount_percent'];
    discountValue = json['discount_value'];
    if (json['customized_header'] != null) {
      customizedHeader = <CustomizedHeader>[];
      json['customized_header'].forEach((v) {
        customizedHeader!.add(CustomizedHeader.fromJson(v));
      });
    }
    ratingDetails = json['rating_details'] != null
        ? RatingDetails.fromJson(json['rating_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['cat_id'] = catId;
    data['sub_cat_id'] = subCatId;
    data['product_unit'] = productUnit;
    data['name'] = name;
    data['description'] = description;
    data['original_price'] = originalPrice;
    data['unit_price'] = unitPrice;
    data['minimum_order'] = minimumOrder;
    data['search_tag'] = searchTag;
    data['highlight_information'] = highlightInformation;
    data['is_discount'] = isDiscount;
    data['is_featured'] = isFeatured;
    data['is_available'] = isAvailable;
    data['code'] = code;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    data['overall_rating'] = overallRating;
    data['touch_count'] = touchCount;
    data['favourite_count'] = favouriteCount;
    data['like_count'] = likeCount;
    data['featured_date'] = featuredDate;
    data['ordering'] = ordering;
    data['ingredient'] = ingredient;
    data['nutrient'] = nutrient;
    data['maximum_order'] = maximumOrder;
    data['dynamic_link'] = dynamicLink;
    data['added_date_str'] = addedDateStr;
    data['trans_status'] = transStatus;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    if (category != null) {
      data['category'] = category!.toJson();
    }
    if (subCategory != null) {
      data['sub_category'] = subCategory!.toJson();
    }
    if (addon != null) {
      data['addon'] = addon!.map((v) => v.toJson()).toList();
    }
    if (specs != null) {
      data['specs'] = specs!.map((v) => v.toJson()).toList();
    }
    data['is_liked'] = isLiked;
    data['is_favourited'] = isFavourited;
    data['image_count'] = imageCount;
    data['comment_header_count'] = commentHeaderCount;
    data['currency_symbol'] = currencySymbol;
    data['currency_short_form'] = currencyShortForm;
    data['discount_amount'] = discountAmount;
    data['discount_percent'] = discountPercent;
    data['discount_value'] = discountValue;
    if (customizedHeader != null) {
      data['customized_header'] =
          customizedHeader!.map((v) => v.toJson()).toList();
    }
    if (ratingDetails != null) {
      data['rating_details'] = ratingDetails!.toJson();
    }
    return data;
  }
}

class DefaultPhoto {
  String? imgId;
  String? imgParentId;
  String? imgType;
  String? imgPath;
  String? imgWidth;
  String? imgHeight;
  String? imgDesc;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? isDefault;

  DefaultPhoto(
      {this.imgId,
      this.imgParentId,
      this.imgType,
      this.imgPath,
      this.imgWidth,
      this.imgHeight,
      this.imgDesc,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.isDefault});

  DefaultPhoto.fromJson(Map<String, dynamic> json) {
    imgId = json['img_id'];
    imgParentId = json['img_parent_id'];
    imgType = json['img_type'];
    imgPath = json['img_path'];
    imgWidth = json['Img_width'];
    imgHeight = json['img_height'];
    imgDesc = json['img_desc'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    isDefault = json['is_default'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['img_id'] = imgId;
    data['img_parent_id'] = imgParentId;
    data['img_type'] = imgType;
    data['img_path'] = imgPath;
    data['Img_width'] = imgWidth;
    data['img_height'] = imgHeight;
    data['img_desc'] = imgDesc;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['is_default'] = isDefault;
    return data;
  }
}

class Category {
  String? id;
  String? name;
  String? status;
  String? addedDate;
  String? updatedDate;
  String? addedUserId;
  String? updatedUserId;
  String? updatedFlag;
  String? touchCount;
  DefaultPhoto? defaultPhoto;
  DefaultPhoto? defaultIcon;

  Category(
      {this.id,
      this.name,
      this.status,
      this.addedDate,
      this.updatedDate,
      this.addedUserId,
      this.updatedUserId,
      this.updatedFlag,
      this.touchCount,
      this.defaultPhoto,
      this.defaultIcon});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    addedDate = json['added_date'];
    updatedDate = json['updated_date'];
    addedUserId = json['added_user_id'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    touchCount = json['touch_count'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    defaultIcon = json['default_icon'] != null
        ? DefaultPhoto.fromJson(json['default_icon'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['updated_date'] = updatedDate;
    data['added_user_id'] = addedUserId;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    data['touch_count'] = touchCount;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    if (defaultIcon != null) {
      data['default_icon'] = defaultIcon!.toJson();
    }
    return data;
  }
}

class SubCategory {
  String? id;
  String? name;
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? catId;
  String? updatedUserId;
  String? updatedFlag;
  DefaultPhoto? defaultPhoto;
  DefaultPhoto? defaultIcon;

  SubCategory(
      {this.id,
      this.name,
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.catId,
      this.updatedUserId,
      this.updatedFlag,
      this.defaultPhoto,
      this.defaultIcon});

  SubCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    catId = json['cat_id'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    defaultIcon = json['default_icon'] != null
        ? DefaultPhoto.fromJson(json['default_icon'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['cat_id'] = catId;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    if (defaultIcon != null) {
      data['default_icon'] = defaultIcon!.toJson();
    }
    return data;
  }
}

class Addon {
  String? id;
  String? name;
  String? description;
  String? price;
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;
  DefaultPhoto? defaultPhoto;

  Addon(
      {this.id,
      this.name,
      this.description,
      this.price,
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag,
      this.defaultPhoto});

  Addon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    price = json['price'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['price'] = price;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    return data;
  }
}

class Specs {
  String? id;
  String? productId;
  String? name;
  String? description;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;

  Specs(
      {this.id,
      this.productId,
      this.name,
      this.description,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag});

  Specs.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    name = json['name'];
    description = json['description'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['product_id'] = productId;
    data['name'] = name;
    data['description'] = description;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    return data;
  }
}

class CustomizedHeader {
  String? id;
  String? productId;
  String? name;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;
  List<CustomizedDetail>? customizedDetail;

  CustomizedHeader(
      {this.id,
      this.productId,
      this.name,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag,
      this.customizedDetail});

  CustomizedHeader.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    name = json['name'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    if (json['customized_detail'] != null) {
      customizedDetail = <CustomizedDetail>[];
      json['customized_detail'].forEach((v) {
        customizedDetail!.add(CustomizedDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['product_id'] = productId;
    data['name'] = name;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    if (customizedDetail != null) {
      data['customized_detail'] =
          customizedDetail!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CustomizedDetail {
  String? id;
  String? headerId;
  String? productId;
  String? name;
  String? additionalPrice;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;

  CustomizedDetail(
      {this.id,
      this.headerId,
      this.productId,
      this.name,
      this.additionalPrice,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag});

  CustomizedDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    headerId = json['header_id'];
    productId = json['product_id'];
    name = json['name'];
    additionalPrice = json['additional_price'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['header_id'] = headerId;
    data['product_id'] = productId;
    data['name'] = name;
    data['additional_price'] = additionalPrice;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    return data;
  }
}

class RatingDetails {
  String? fiveStarCount;
  String? fiveStarPercent;
  String? fourStarCount;
  String? fourStarPercent;
  String? threeStarCount;
  String? threeStarPercent;
  String? twoStarCount;
  String? twoStarPercent;
  String? oneStarCount;
  String? oneStarPercent;
  String? totalRatingCount;
  String? totalRatingValue;

  RatingDetails(
      {this.fiveStarCount,
      this.fiveStarPercent,
      this.fourStarCount,
      this.fourStarPercent,
      this.threeStarCount,
      this.threeStarPercent,
      this.twoStarCount,
      this.twoStarPercent,
      this.oneStarCount,
      this.oneStarPercent,
      this.totalRatingCount,
      this.totalRatingValue});

  RatingDetails.fromJson(Map<String, dynamic> json) {
    fiveStarCount = json['five_star_count'];
    fiveStarPercent = json['five_star_percent'];
    fourStarCount = json['four_star_count'];
    fourStarPercent = json['four_star_percent'];
    threeStarCount = json['three_star_count'];
    threeStarPercent = json['three_star_percent'];
    twoStarCount = json['two_star_count'];
    twoStarPercent = json['two_star_percent'];
    oneStarCount = json['one_star_count'];
    oneStarPercent = json['one_star_percent'];
    totalRatingCount = json['total_rating_count'];
    totalRatingValue = json['total_rating_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['five_star_count'] = fiveStarCount;
    data['five_star_percent'] = fiveStarPercent;
    data['four_star_count'] = fourStarCount;
    data['four_star_percent'] = fourStarPercent;
    data['three_star_count'] = threeStarCount;
    data['three_star_percent'] = threeStarPercent;
    data['two_star_count'] = twoStarCount;
    data['two_star_percent'] = twoStarPercent;
    data['one_star_count'] = oneStarCount;
    data['one_star_percent'] = oneStarPercent;
    data['total_rating_count'] = totalRatingCount;
    data['total_rating_value'] = totalRatingValue;
    return data;
  }
}
