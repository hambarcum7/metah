class AllCategory {
  String? id;
  String name = "";
  String? status;
  String? addedDate;
  String? updatedDate;
  String? addedUserId;
  String? updatedUserId;
  String? updatedFlag;
  String? touchCount;
  String? addedDateStr;
  DefaultPhoto? defaultPhoto;
  DefaultPhoto? defaultIcon;

  AllCategory(
      {this.id,
      required this.name,
      this.status,
      this.addedDate,
      this.updatedDate,
      this.addedUserId,
      this.updatedUserId,
      this.updatedFlag,
      this.touchCount,
      this.addedDateStr,
      this.defaultPhoto,
      this.defaultIcon});

  AllCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    addedDate = json['added_date'];
    updatedDate = json['updated_date'];
    addedUserId = json['added_user_id'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    touchCount = json['touch_count'];
    addedDateStr = json['added_date_str'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    defaultIcon = json['default_icon'] != null
        ? DefaultPhoto.fromJson(json['default_icon'])
        : null;
  }

 
}

class DefaultPhoto {
  String? imgId;
  String? imgParentId;
  String? imgType;
  String imgPath = "";
  String? imgWidth;
  String? imgHeight;
  String? imgDesc;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? isDefault;

  DefaultPhoto(
      {
      this.imgId,
      this.imgParentId,
      this.imgType,
      required this.imgPath,
      this.imgWidth,
      this.imgHeight,
      this.imgDesc,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.isDefault});

  DefaultPhoto.fromJson(Map<String, dynamic> json) {
    imgId = json['img_id'];
    imgParentId = json['img_parent_id'];
    imgType = json['img_type'];
    imgPath = json['img_path'];
    imgWidth = json['Img_width'];
    imgHeight = json['img_height'];
    imgDesc = json['img_desc'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    isDefault = json['is_default'];
  }

}