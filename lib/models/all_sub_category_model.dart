class AllSubCategory {
  String? id;
  String name = '';
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? catId;
  String? updatedUserId;
  String? updatedFlag;
  String? addedDateStr;
  DefaultPhotoAllCategory? defaultPhoto;
  DefaultPhotoAllCategory? defaultIcon;

  AllSubCategory(
      {this.id,
      this.name = '',
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.catId,
      this.updatedUserId,
      this.updatedFlag,
      this.addedDateStr,
      this.defaultPhoto,
      this.defaultIcon});

  AllSubCategory.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    catId = json['cat_id'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    addedDateStr = json['added_date_str'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhotoAllCategory.fromJson(json['default_photo'])
        : null;
    defaultIcon = json['default_icon'] != null
        ? DefaultPhotoAllCategory.fromJson(json['default_icon'])
        : null;
  }

  
}

class DefaultPhotoAllCategory {
  String? imgId;
  String? imgParentId;
  String? imgType;
  String imgPath = '';
  String? imgWidth;
  String? imgHeight;
  String? imgDesc;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? isDefault;

  DefaultPhotoAllCategory(
      {this.imgId,
      this.imgParentId,
      this.imgType,
      this.imgPath = '',
      this.imgWidth,
      this.imgHeight,
      this.imgDesc,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.isDefault});

  DefaultPhotoAllCategory.fromJson(Map<String, dynamic> json) {
    imgId = json['img_id'];
    imgParentId = json['img_parent_id'];
    imgType = json['img_type'];
    imgPath = json['img_path'];
    imgWidth = json['Img_width'];
    imgHeight = json['img_height'];
    imgDesc = json['img_desc'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    isDefault = json['is_default'];
  }
}
