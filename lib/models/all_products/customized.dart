

class CustomizedHeader {
  String? id;
  String? productId;
  String? name;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;
  List<CustomizedDetail>? customizedDetail;

  CustomizedHeader(
      {this.id,
      this.productId,
      this.name,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag,
      this.customizedDetail});

  CustomizedHeader.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    name = json['name'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    if (json['customized_detail'] != null) {
      customizedDetail = <CustomizedDetail>[];
      json['customized_detail'].forEach((v) {
        customizedDetail!.add(CustomizedDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['product_id'] = productId;
    data['name'] = name;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    if (customizedDetail != null) {
      data['customized_detail'] =
          customizedDetail!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CustomizedDetail {
  String? id;
  String? headerId;
  String? productId;
  String? name;
  String? additionalPrice;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;

  CustomizedDetail(
      {this.id,
      this.headerId,
      this.productId,
      this.name,
      this.additionalPrice,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag});

  CustomizedDetail.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    headerId = json['header_id'];
    productId = json['product_id'];
    name = json['name'];
    additionalPrice = json['additional_price'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['header_id'] = headerId;
    data['product_id'] = productId;
    data['name'] = name;
    data['additional_price'] = additionalPrice;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    return data;
  }
}

class RatingDetails {
  String? fiveStarCount;
  String? fiveStarPercent;
  String? fourStarCount;
  String? fourStarPercent;
  String? threeStarCount;
  String? threeStarPercent;
  String? twoStarCount;
  String? twoStarPercent;
  String? oneStarCount;
  String? oneStarPercent;
  String? totalRatingCount;
  String? totalRatingValue;

  RatingDetails(
      {this.fiveStarCount,
      this.fiveStarPercent,
      this.fourStarCount,
      this.fourStarPercent,
      this.threeStarCount,
      this.threeStarPercent,
      this.twoStarCount,
      this.twoStarPercent,
      this.oneStarCount,
      this.oneStarPercent,
      this.totalRatingCount,
      this.totalRatingValue});

  RatingDetails.fromJson(Map<String, dynamic> json) {
    fiveStarCount = json['five_star_count'];
    fiveStarPercent = json['five_star_percent'];
    fourStarCount = json['four_star_count'];
    fourStarPercent = json['four_star_percent'];
    threeStarCount = json['three_star_count'];
    threeStarPercent = json['three_star_percent'];
    twoStarCount = json['two_star_count'];
    twoStarPercent = json['two_star_percent'];
    oneStarCount = json['one_star_count'];
    oneStarPercent = json['one_star_percent'];
    totalRatingCount = json['total_rating_count'];
    totalRatingValue = json['total_rating_value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['five_star_count'] = fiveStarCount;
    data['five_star_percent'] = fiveStarPercent;
    data['four_star_count'] = fourStarCount;
    data['four_star_percent'] = fourStarPercent;
    data['three_star_count'] = threeStarCount;
    data['three_star_percent'] = threeStarPercent;
    data['two_star_count'] = twoStarCount;
    data['two_star_percent'] = twoStarPercent;
    data['one_star_count'] = oneStarCount;
    data['one_star_percent'] = oneStarPercent;
    data['total_rating_count'] = totalRatingCount;
    data['total_rating_value'] = totalRatingValue;
    return data;
  }
}
