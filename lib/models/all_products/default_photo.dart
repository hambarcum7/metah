class DefaultPhoto {
  String? imgId;
  String? imgParentId;
  String? imgType;
  String? imgPath;
  String? imgWidth;
  String? imgHeight;
  String? imgDesc;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? isDefault;

  DefaultPhoto(
      {this.imgId,
      this.imgParentId,
      this.imgType,
      this.imgPath,
      this.imgWidth,
      this.imgHeight,
      this.imgDesc,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.isDefault});

  DefaultPhoto.fromJson(Map<String, dynamic> json) {
    imgId = json['img_id'];
    imgParentId = json['img_parent_id'];
    imgType = json['img_type'];
    imgPath = json['img_path'];
    imgWidth = json['Img_width'];
    imgHeight = json['img_height'];
    imgDesc = json['img_desc'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    isDefault = json['is_default'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['img_id'] = imgId;
    data['img_parent_id'] = imgParentId;
    data['img_type'] = imgType;
    data['img_path'] = imgPath;
    data['Img_width'] = imgWidth;
    data['img_height'] = imgHeight;
    data['img_desc'] = imgDesc;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['is_default'] = isDefault;
    return data;
  }
}