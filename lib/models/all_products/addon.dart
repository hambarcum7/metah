import 'package:vanfood/models/all_products/default_photo.dart';

class Addon {
  String? id;
  String? name;
  String? description;
  String? price;
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;
  DefaultPhoto? defaultPhoto;

  Addon(
      {this.id,
      this.name,
      this.description,
      this.price,
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag,
      this.defaultPhoto});

  Addon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    description = json['description'];
    price = json['price'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['description'] = description;
    data['price'] = price;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    return data;
  }
}
