import 'package:vanfood/models/all_products/addon.dart';
import 'package:vanfood/models/all_products/category.dart';
import 'package:vanfood/models/all_products/customized.dart';
import 'package:vanfood/models/all_products/default_photo.dart';
import 'package:vanfood/models/all_products/specs.dart';

class Allproducts {
  String? id;
  String? catId;
  String? subCatId;
  String? productUnit;
  String? name;
  String? description;
  String? originalPrice;
  String? unitPrice;
  String? minimumOrder;
  String? searchTag;
  String? highlightInformation;
  String? isDiscount;
  String? isFeatured;
  String? isAvailable;
  String? code;
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? updatedUserId;
  String? updatedFlag;
  String? overallRating;
  String? touchCount;
  String? favouriteCount;
  String? likeCount;
  String? featuredDate;
  String? ordering;
  String? ingredient;
  String? nutrient;
  String? maximumOrder;
  String? dynamicLink;
  String? transStatus;
  DefaultPhoto? defaultPhoto;
  Category? category;
  SubCategory? subCategory;
  List<Addon>? addon;
  List<Specs>? specs;
  String? isLiked;
  String? isFavourited;
  String? imageCount;
  String? commentHeaderCount;
  String? currencySymbol;
  String? currencyShortForm;
  String? discountAmount;
  String? discountPercent;
  String? discountValue;
  List<CustomizedHeader>? customizedHeader;
  RatingDetails? ratingDetails;

  Allproducts(
      {this.id,
      this.catId,
      this.subCatId,
      this.productUnit,
      this.name,
      this.description,
      this.originalPrice,
      this.unitPrice,
      this.minimumOrder,
      this.searchTag,
      this.highlightInformation,
      this.isDiscount,
      this.isFeatured,
      this.isAvailable,
      this.code,
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.updatedUserId,
      this.updatedFlag,
      this.overallRating,
      this.touchCount,
      this.favouriteCount,
      this.likeCount,
      this.featuredDate,
      this.ordering,
      this.ingredient,
      this.nutrient,
      this.maximumOrder,
      this.dynamicLink,
      this.transStatus,
      this.defaultPhoto,
      this.category,
      this.subCategory,
      this.addon,
      this.specs,
      this.isLiked,
      this.isFavourited,
      this.imageCount,
      this.commentHeaderCount,
      this.currencySymbol,
      this.currencyShortForm,
      this.discountAmount,
      this.discountPercent,
      this.discountValue,
      this.customizedHeader,
      this.ratingDetails});

  Allproducts.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    catId = json['cat_id'];
    subCatId = json['sub_cat_id'];
    productUnit = json['product_unit'];
    name = json['name'];
    description = json['description'];
    originalPrice = json['original_price'];
    unitPrice = json['unit_price'];
    minimumOrder = json['minimum_order'];
    searchTag = json['search_tag'];
    highlightInformation = json['highlight_information'];
    isDiscount = json['is_discount'];
    isFeatured = json['is_featured'];
    isAvailable = json['is_available'];
    code = json['code'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    overallRating = json['overall_rating'];
    touchCount = json['touch_count'];
    favouriteCount = json['favourite_count'];
    likeCount = json['like_count'];
    featuredDate = json['featured_date'];
    ordering = json['ordering'];
    ingredient = json['ingredient'];
    nutrient = json['nutrient'];
    maximumOrder = json['maximum_order'];
    dynamicLink = json['dynamic_link'];
    transStatus = json['trans_status'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    category =
        json['category'] != null ? Category.fromJson(json['category']) : null;
    subCategory = json['sub_category'] != null
        ? SubCategory.fromJson(json['sub_category'])
        : null;

    if (json['addon'] != null) {
      addon = <Addon>[];
      json['addon'].forEach((v) {
        addon!.add(Addon.fromJson(v));
      });
    }
    if (json['specs'] != null) {
      specs = <Specs>[];
      json['specs'].forEach((v) {
        specs!.add(Specs.fromJson(v));
      });
    }
    isLiked = json['is_liked'];
    isFavourited = json['is_favourited'];
    imageCount = json['image_count'];
    commentHeaderCount = json['comment_header_count'];
    currencySymbol = json['currency_symbol'];
    currencyShortForm = json['currency_short_form'];
    discountAmount = json['discount_amount'];
    discountPercent = json['discount_percent'];
    discountValue = json['discount_value'];
    if (json['customized_header'] != null) {
      customizedHeader = <CustomizedHeader>[];
      json['customized_header'].forEach((v) {
        customizedHeader!.add(CustomizedHeader.fromJson(v));
      });
    }
    ratingDetails = json['rating_details'] != null
        ? RatingDetails.fromJson(json['rating_details'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['cat_id'] = catId;
    data['sub_cat_id'] = subCatId;
    data['product_unit'] = productUnit;
    data['name'] = name;
    data['description'] = description;
    data['original_price'] = originalPrice;
    data['unit_price'] = unitPrice;
    data['minimum_order'] = minimumOrder;
    data['search_tag'] = searchTag;
    data['highlight_information'] = highlightInformation;
    data['is_discount'] = isDiscount;
    data['is_featured'] = isFeatured;
    data['is_available'] = isAvailable;
    data['code'] = code;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    data['overall_rating'] = overallRating;
    data['touch_count'] = touchCount;
    data['favourite_count'] = favouriteCount;
    data['like_count'] = likeCount;
    data['featured_date'] = featuredDate;
    data['ordering'] = ordering;
    data['ingredient'] = ingredient;
    data['nutrient'] = nutrient;
    data['maximum_order'] = maximumOrder;
    data['dynamic_link'] = dynamicLink;
    data['trans_status'] = transStatus;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    if (category != null) {
      data['category'] = category!.toJson();
    }
    if (subCategory != null) {
      data['sub_category'] = subCategory!.toJson();
    }
    if (addon != null) {
      data['addon'] = addon!.map((v) => v.toJson()).toList();
    }
    if (specs != null) {
      data['specs'] = specs!.map((v) => v.toJson()).toList();
    }
    data['is_liked'] = isLiked;
    data['is_favourited'] = isFavourited;
    data['image_count'] = imageCount;
    data['comment_header_count'] = commentHeaderCount;
    data['currency_symbol'] = currencySymbol;
    data['currency_short_form'] = currencyShortForm;
    data['discount_amount'] = discountAmount;
    data['discount_percent'] = discountPercent;
    data['discount_value'] = discountValue;
    if (customizedHeader != null) {
      data['customized_header'] =
          customizedHeader!.map((v) => v.toJson()).toList();
    }
    if (ratingDetails != null) {
      data['rating_details'] = ratingDetails!.toJson();
    }
    return data;
  }
}