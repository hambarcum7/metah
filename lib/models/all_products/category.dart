import 'package:vanfood/models/all_products/default_photo.dart';

class Category {
  String? id;
  String? name;
  String? status;
  String? addedDate;
  String? updatedDate;
  String? addedUserId;
  String? updatedUserId;
  String? updatedFlag;
  String? touchCount;
  DefaultPhoto? defaultPhoto;
  DefaultPhoto? defaultIcon;

  Category(
      {this.id,
      this.name,
      this.status,
      this.addedDate,
      this.updatedDate,
      this.addedUserId,
      this.updatedUserId,
      this.updatedFlag,
      this.touchCount,
      this.defaultPhoto,
      this.defaultIcon});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    addedDate = json['added_date'];
    updatedDate = json['updated_date'];
    addedUserId = json['added_user_id'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    touchCount = json['touch_count'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    defaultIcon = json['default_icon'] != null
        ? DefaultPhoto.fromJson(json['default_icon'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['updated_date'] = updatedDate;
    data['added_user_id'] = addedUserId;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    data['touch_count'] = touchCount;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    if (defaultIcon != null) {
      data['default_icon'] = defaultIcon!.toJson();
    }
    return data;
  }
}

class SubCategory {
  String? id;
  String? name;
  String? status;
  String? addedDate;
  String? addedUserId;
  String? updatedDate;
  String? catId;
  String? updatedUserId;
  String? updatedFlag;
  DefaultPhoto? defaultPhoto;
  DefaultPhoto? defaultIcon;

  SubCategory(
      {this.id,
      this.name,
      this.status,
      this.addedDate,
      this.addedUserId,
      this.updatedDate,
      this.catId,
      this.updatedUserId,
      this.updatedFlag,
      this.defaultPhoto,
      this.defaultIcon});

  SubCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    status = json['status'];
    addedDate = json['added_date'];
    addedUserId = json['added_user_id'];
    updatedDate = json['updated_date'];
    catId = json['cat_id'];
    updatedUserId = json['updated_user_id'];
    updatedFlag = json['updated_flag'];
    defaultPhoto = json['default_photo'] != null
        ? DefaultPhoto.fromJson(json['default_photo'])
        : null;
    defaultIcon = json['default_icon'] != null
        ? DefaultPhoto.fromJson(json['default_icon'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['status'] = status;
    data['added_date'] = addedDate;
    data['added_user_id'] = addedUserId;
    data['updated_date'] = updatedDate;
    data['cat_id'] = catId;
    data['updated_user_id'] = updatedUserId;
    data['updated_flag'] = updatedFlag;
    if (defaultPhoto != null) {
      data['default_photo'] = defaultPhoto!.toJson();
    }
    if (defaultIcon != null) {
      data['default_icon'] = defaultIcon!.toJson();
    }
    return data;
  }
}